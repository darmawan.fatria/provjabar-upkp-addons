import datetime
import time
from openerp.osv import osv
from openerp.report import report_sxw

NAMA_BULAN =['FALSE','Januari','Februari','Maret','April','Mei','Juni',
             'Juli','Agustus','September','Oktober','November','Desember']
NAMA_HARI = ['FALSE','Minggu','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu']

class ruang_peserta_object_printout_report_parser(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(ruang_peserta_object_printout_report_parser, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'get_peserta': self._get_peserta,
        })
    
    def _get_peserta(self,ruang,baris,kolom):
        query = """
                select b.name
                from konfigurasi_periode_peserta_ujian a,pendaftaran_upkp_ud b
                where a.ruang_id=%s
                and a.baris = %s and a.kolom = %s
                and b.id=a.upkp_ud_id
                """			
        param = [ruang,baris,kolom]
        
        self.cr.execute(query, param)
        result = self.cr.fetchone()
        if result is None :
            return ' '
        else :
            return result[0]

class ruang_peserta_object_report(osv.AbstractModel):
    _name = 'report.df_upkp_ud_ruang_peserta_report.ruang_peserta_object_printout_report_template'
    _inherit = 'report.abstract_report'
    _template = 'df_upkp_ud_ruang_peserta_report.ruang_peserta_object_printout_report_template'
    _wrapped_report_class = ruang_peserta_object_printout_report_parser