##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2016
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

{
    "name": "Kartu Peserta Ujian Dinas Dan Penyesuaian Kenaikan Pangkat",
    "version": "1.0",
    "author": "Darmawan Fatriananda",
    "category": "Kepegawaian/Kenaikan Pangkat",
    "description": """Cetak Kartu Peserta Ujian Dinas dan Penyesuaian Kenaikan Pangkat""",
    "website" : "http://-",
    "license" : "GPL-3",
    "depends": [
                "df_upkp_ud",
                ],
    'data': [
            "kartu_peserta_upkp_ud_view.xml",
            "report/kartu_peserta_object_printout_report.xml",
            "report/kartu_peserta_object_printout_report_template.xml",
             ],
    #'demo_xml': [],
    'installable': True,
    #'active': False,
}
