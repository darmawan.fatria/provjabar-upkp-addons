import datetime
import time
from openerp.osv import osv
from openerp.report import report_sxw

NAMA_BULAN =['FALSE','Januari','Februari','Maret','April','Mei','Juni',
             'Juli','Agustus','September','Oktober','November','Desember']
NAMA_HARI = ['FALSE','Minggu','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu']
class kep_gubernur_object_printout_report_parser(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(kep_gubernur_object_printout_report_parser, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'get_param': self._get_param,
        })
        
    def _get_param(self,ids,index):
        _ids = str(ids)
        query = """
                select other from konfigurasi_umum_upkp_ud where
                key='KEPGUB'
                """			
        param = [_ids]
        
        self.cr.execute(query, param)
            
        result = self.cr.fetchone()[0]
        
        explode_val = result.split('|')
        value =  explode_val[index]
            
        return value

        
        
class kep_gubernur_object_report(osv.AbstractModel):
    _name = 'report.df_upkp_ud_kep_gubernur_report.kep_gubernur_object_printout_report_template'
    _inherit = 'report.abstract_report'
    _template = 'df_upkp_ud_kep_gubernur_report.kep_gubernur_object_printout_report_template'
    _wrapped_report_class = kep_gubernur_object_printout_report_parser