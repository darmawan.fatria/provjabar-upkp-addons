import datetime
import time
from openerp.osv import osv
from openerp.report import report_sxw

NAMA_BULAN =['FALSE','Januari','Februari','Maret','April','Mei','Juni',
             'Juli','Agustus','September','Oktober','November','Desember']
NAMA_HARI = ['FALSE','Minggu','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu']
class surat_lulus_object_printout_report_parser(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(surat_lulus_object_printout_report_parser, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'get_date': self._get_date,
            'get_param': self._get_param,
            'get_sum': self._get_sum,
            'get_no_urut' : self._get_no_urut,
        })
        
    def _get_param(self,index):
        query = """
                select other from konfigurasi_umum_upkp_ud where
                key = 'TANDALULUS'
                """			
        param = []
        self.cr.execute(query, param)
        result = self.cr.fetchone()[0]
        
        explode_val = result.split('|')
        value =  explode_val[index]
        
        return value
    
    def _get_sum(self,ids):
        query = """
                select sum(nilai) from hasil_materi_ujian where pendaftaran_id = %s
                """			
        param = [ids]
        self.cr.execute(query, param)
        result = self.cr.fetchone()[0]
        
        return result

    def _get_date(self):
        now = datetime.datetime.now()
        current_year = int(now.strftime("%Y"))
        current_month = int(now.strftime("%m"))
        current_date = now.strftime("%d")
        date_val = " %s %s" % (NAMA_BULAN[current_month],current_year)
        return date_val
    
    def _get_no_urut(self,ids,materi_ids) :
        query = """
                select count(*) from hasil_materi_ujian where pendaftaran_id = %s and id<=%s
                """			
        param = [ids,materi_ids]
        self.cr.execute(query, param)
        result = self.cr.fetchone()[0]
        
        return result
        
class surat_lulus_object_report(osv.AbstractModel):
    _name = 'report.df_upkp_ud_surat_lulus_report.surat_lulus_object_printout_report_template'
    _inherit = 'report.abstract_report'
    _template = 'df_upkp_ud_surat_lulus_report.surat_lulus_object_printout_report_template'
    _wrapped_report_class = surat_lulus_object_printout_report_parser