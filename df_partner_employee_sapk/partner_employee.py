# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://www.asdarfat.wordpress.com.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv
from datetime import date,datetime,timedelta
from openerp import SUPERUSER_ID
import time
from email.utils import formataddr

#==== Res mail_message
class mail_message(osv.Model):
    _inherit = 'mail.message'

    def _get_default_from(self, cr, uid, context=None):
        this = self.pool.get('res.users').browse(cr, SUPERUSER_ID, uid, context=context)
        if this.alias_name and this.alias_domain:
            return formataddr((this.name, '%s@%s' % (this.alias_name, this.alias_domain)))
        elif this.email:
            return formataddr((this.name, this.email))
        else :
            return formataddr((this.name, 'upkpadmin@skp.jabarprov.go.id'))
            #raise osv.except_osv(_('Invalid Action!'), _("Unable to send email, please configure the sender's email address or alias."))
mail_message()
# ====================== res partner ================================= #

class res_partner(osv.Model):
    _inherit = 'res.partner'
    _description ='Data Kepegawaian'
    


    def _get_fullname(self, cr, uid, ids, field_names, args, context=None):
        res = {}
        for employee in self.browse(cr, uid, ids, context=context):
            fullname=employee.name
            
            if employee.gelar_depan and employee.gelar_depan.name :
                fullname=employee.gelar_depan.name + ' ' +fullname
            if employee.gelar_blk and employee.gelar_blk.name :
                fullname=fullname + ' '+employee.gelar_blk.name
           
            res[employee.id] = fullname
        return res
    def _get_lower_fullname(self, cr, uid, ids, field_names, args, context=None):
        res = {}
        for employee in self.browse(cr, uid, ids, context=context):
            if employee.name:
                res[employee.id] = employee.name.lower()
            
        return res

    

    
    def _get_pendidikan_terakhir(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}


        for record in self.browse(cr,uid,ids,context):
            pendidikan_terakhir=''
            if record.jenjang_pendidikan_id and record.jurusan_id:
                pendidikan_terakhir = record.jenjang_pendidikan_id.name +' '+record.jurusan_id.name
            elif record.jenjang_pendidikan_id :
                pendidikan_terakhir = record.jenjang_pendidikan_id.name

            res[record.id]=pendidikan_terakhir
        return res

    def _get_usia(self, cr, uid, ids, name, arg, context=None):
        if context is None:
            context = {}
        res = {}
        
        usia=0
        today = date.today()
        for record in self.read(cr,uid,ids,["id","tanggal_lahir"],context):
            if record['tanggal_lahir'] :
                tahun_lahir = time.strftime('%Y', time.strptime(record['tanggal_lahir'],'%Y-%m-%d'))
                usia = today.year - int(tahun_lahir)
            res[record['id']]=usia
        return res

    job_type_list =[('struktural', 'Jabatan Struktural'), ('jft', 'Jabatan Fungsional Tertentu'), ('jfu', 'Jabatan Fungsional Umum')]
    
    
    _columns = {
        'nip'     : fields.char('NIP',size=20),
        'nip_lama'     : fields.char('NIP Lama',size=20),
        'fullname' :fields.function(_get_fullname, method=True, string='Nama Lengkap Beserta Gelar', type='char', readonly=True,store=False),
        'lower_fullname' :fields.function(_get_lower_fullname, method=True, string='Nama Lengkap Beserta Gelar', type='char', readonly=True,store=True),
        'gelar_depan': fields.many2one('partner.employee.title', 'Gelar Depan',domain="[('title_type','=','gelar_depan')]"),
        'gelar_blk': fields.many2one('partner.employee.title', 'Gelar Belakang',domain="[('title_type','=','gelar_belakang')]"),

        'tempat_lahir'     : fields.char('Tempat Lahir',size=128),
        'tanggal_lahir'     : fields.date('Tanggal Lahir'),
        'agama'     : fields.selection([('islam', 'Islam'),  
                                                      ('katolik', 'Katolik'), 
                                                      ('protestan', 'Protestan'), 
                                                      ('hindu', 'Hindu'), 
                                                      ('budha', 'Budha') 
                                                     ,
                                                     ],'Agama', 
                                                     ),
        'golongan_awal_id': fields.many2one('partner.employee.golongan', 'Golongan Awal',required=False),
         'tmt_cpns'     : fields.date('TMT CPNS'),
         'tmt_pns'     : fields.date('TMT PNS'),


         'jenis_kelamin': fields.selection([('L', 'Laki-Laki'), ('P', 'Perempuan'), ], 'Jenis Kelamin'),
         'golongan_id': fields.many2one('partner.employee.golongan', 'Golongan Akhir',required=False),
          'tmt_golongan'     : fields.date('TMT'),
          'masa_kerja_thn' : fields.integer('Masa Kerja (Thn)'),
          'masa_kerja_bln' : fields.integer('Masa Kerja (Bln)'),
          'masa_kerja_thn_fn' : fields.integer('Masa Kerja (Thn)'),
          'masa_kerja_bln_fn' : fields.integer('Masa Kerja (Bln)'),

          'eselon_id': fields.many2one('partner.employee.eselon', 'Eselon'),
          'tmt_job'     : fields.date('TMT'),
          'job_id': fields.many2one('partner.employee.job', 'Jabatan',required=False),
          'job_type': fields.selection(job_type_list, 'Jenis Jabatan',required=False),
          'department_id': fields.many2one('partner.employee.department', 'Unit Kerja'),
          'parent_department_id': fields.many2one('partner.employee.department', 'Unit Kerja'),


        'nama_sekolah_id': fields.many2one('partner.employee.school', 'Sekolah',required=False),
        'jurusan_id': fields.many2one('partner.employee.study', 'Jurusan / Program Studi',),
        'jenjang_pendidikan_id': fields.many2one('partner.employee.study.type', 'Jenjang Pendidikan',required=False,),
         'pendidikan_terakhir': fields.function(_get_pendidikan_terakhir, method=True,readonly=True , store=False,
                                         type="char",string='Pendidikan Terakhir'),
        'tahun_lulus': fields.char("Tahun Lulus", size=4 ,required=False),
        'ipk': fields.float("IPK", ),

         'employee_state'     : fields.char('Jenis Peg.',size=120),


         'is_head_of_all': fields.boolean('Kepala Daerah', ),
        'is_kepala_opd': fields.boolean('Kepala Badan/Dinas/SKPD', ),
         'is_share_users': fields.boolean('Shared', ),
         'data_preparation': fields.boolean('Data Preparation', ),

         'usia': fields.function(_get_usia, method=True,readonly=True , store=False,
                                         type="integer",string='Usia'),


    }
    _defaults = {
        'employee' : True,
        'is_head_of_all' : False,
        'data_preparation':False,
    }
    _sql_constraints = [
         ('name_uniq', 'unique (nip)',
             'Data Tidak Bisa Dimasukan, NIP Sudah Tersedia')
     ]
    def init(self, cr):
        pass

res_partner()
