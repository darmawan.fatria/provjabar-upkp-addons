##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2016
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

{
    "name": "Data Kepegawaian Perangkat Daerah",
    "version": "1.0",
    "author": "Darmawan Fatriananda",
    "category": "Partner/Kepegawaian",
    "description": """
        Berhubungan dengan Data Kepegawaian
    """,
    "website" : "http://-",
    "license" : "GPL-3",
    "depends": [
                "base","mail",
                ],
    'data': [      "security/partner_employee_security.xml",
                  'security/ir.model.access.csv',
                   "partner_employee_config_view.xml",
                   "partner_employee_view.xml",

                   
                   ],
    #'demo_xml': [],
    'installable': True,
    #'active': False,
}
