# -*- encoding: utf-8 -*-
##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2014 <http://www.asdarfat.wordpress.com.com>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv
from datetime import datetime,timedelta
import time
from mx import DateTime

class partner_employee_department(osv.Model):
    _name = 'partner.employee.department'
    _description    ="Unit Kerja"
   
    _columns = {
        'name': fields.char("Nama Unit Kerja", size=256, required=True),
        'code': fields.char("Kode Unit Kerja", size=12, ),
        'parent_id': fields.many2one('partner.employee.department', 'Induk Unit Kerja'),
        'company_id': fields.many2one('res.company', 'OPD/SKPD',required=True),
        'description': fields.text("Deskripsi"),
    }
    def _check_unique_name_insesitive(self, cr, uid, ids, context=None):
        sr_ids = self.search(cr, 1 ,[], context=context)
        lst = [
                x.name.lower().strip() for x in self.browse(cr, uid, sr_ids, context=context)
                if x.name and x.id not in ids
              ]
        for self_obj in self.browse(cr, uid, ids, context=context):
            if self_obj.name and self_obj.name.lower() and self_obj.name.strip() and self_obj.name.lower().strip() in  lst:
                return False
        return True

    #_constraints = [(_check_unique_name_insesitive, 'Error: Nama Sudah Tersedia ', ['name','company_id'])]

partner_employee_department()

class partner_employee_eselon(osv.Model):
    _name = "partner.employee.eselon"
    _description = "Eselon Pegawai"
    _columns = {
        'code': fields.char("Kode", size=8, required=True),
        'name': fields.char("Eselon", size=25,required=True ),
        'sequence': fields.integer("Sequence",),
        'description': fields.text("Deskripsi" )        
    }
    
    def _check_unique_name_insesitive(self, cr, uid, ids, context=None):
        sr_ids = self.search(cr, 1 ,[], context=context)
        lst = [
                x.name.lower().strip() for x in self.browse(cr, uid, sr_ids, context=context)
                if x.name and x.id not in ids
              ]
        for self_obj in self.browse(cr, uid, ids, context=context):
            if self_obj.name and self_obj.name.lower() and self_obj.name.strip() and self_obj.name.lower().strip() in  lst:
                return False
        return True

    _constraints = [(_check_unique_name_insesitive, 'Error: Nama Sudah Tersedia ', ['name'])]
partner_employee_eselon()

class partner_employee_biro(osv.Model):
    _name = "partner.employee.biro"
    _description = "Biro Pegawai"
    _columns = {
        'name': fields.char("Biro", size=25,required=True ),
        'description': fields.text("Deskripsi"),
                
    }
    def _check_unique_name_insesitive(self, cr, uid, ids, context=None):
        sr_ids = self.search(cr, 1 ,[], context=context)
        lst = [
                x.name.lower().strip() for x in self.browse(cr, uid, sr_ids, context=context)
                if x.name and x.id not in ids
              ]
        for self_obj in self.browse(cr, uid, ids, context=context):
            if self_obj.name and self_obj.name.lower() and self_obj.name.strip() and self_obj.name.lower().strip() in  lst:
                return False
        return True

    _constraints = [(_check_unique_name_insesitive, 'Error: Nama Sudah Tersedia ', ['name'])]
partner_employee_eselon()
class partner_employee_golongan(osv.Model):
    _name = "partner.employee.golongan"
    _description = "Golongan Pegawai"
    _columns = {
        'name': fields.char("Golongan/Ruang", size=12, required=True),
        'golongan': fields.integer("Golongan", required=True ),
        'ruang': fields.char("Ruang", size=12, ),
        'description': fields.char("Deskripsi", size=256, ),
        'level': fields.integer("Level", required=True ),
    }
    def _check_unique_name_insesitive(self, cr, uid, ids, context=None):
        sr_ids = self.search(cr, 1 ,[], context=context)
        lst = [
                x.name.lower().strip() for x in self.browse(cr, uid, sr_ids, context=context)
                if x.name and x.id not in ids
              ]
        for self_obj in self.browse(cr, uid, ids, context=context):
            if self_obj.name and self_obj.name.lower() and self_obj.name.strip() and self_obj.name.lower().strip() in  lst:
                return False
        return True

    _constraints = [(_check_unique_name_insesitive, 'Error: Nama Sudah Tersedia ', ['name'])]
partner_employee_golongan()

class partner_employee_job(osv.Model):
    _name = "partner.employee.job"
    _description = "Jabatan"
    _columns = {
        'name': fields.char('Jabatan', size=500, required=True ),
        'code': fields.char('Kode', size=12, required=False ),
        'job_type': fields.selection([('struktural', 'Jabatan Struktural'), ('jft', 'Jabatan Fungsional Tertentu'), ('jfu', 'Jabatan Fungsional Umum')], 'Jenis Jabatan', required=False ),
        'name_alias': fields.char('Alias Jabatan ', size=350, help="Nama Jabatan Yang Terlampir Dalam Surat Sesuai Dengan Standarisasi Nomenteratur"),
        'description': fields.text("Deskripsi Pekerjaan"),
        'kualifikasi': fields.text('Kualifikasi Pendidikan'),
     }
    def _check_unique_name_insesitive(self, cr, uid, ids, context=None):
        sr_ids = self.search(cr, 1 ,[], context=context)
        lst = [
                x.name.lower().strip() for x in self.browse(cr, uid, sr_ids, context=context)
                if x.name and x.id not in ids
              ]
        for self_obj in self.browse(cr, uid, ids, context=context):
            if self_obj.name and self_obj.name.lower() and self_obj.name.strip() and self_obj.name.lower().strip() in  lst:
                return False
        return True

    #_constraints = [(_check_unique_name_insesitive, 'Error: Nama Sudah Tersedia ', ['name'])]
partner_employee_job()


class partner_employee_title(osv.Model):
    _name = "partner.employee.title"
    _description = "Gelar Depan Dan Belakang"
    _columns = {
        'name': fields.char("Nama Gelar", size=64, required=True),
        'title_type': fields.selection([('gelar_depan', 'Gelar Depan'), ('gelar_belakang', 'Gelar Belakang')], 'Jenis Gelar', required=True,
            help="Contoh Gelar Depan :Dr. H.;H | Contoh Gelar Belakang :S.STP.,MAP;AM.KG "),
    }
    def _check_unique_name_insesitive(self, cr, uid, ids, context=None):
        sr_ids = self.search(cr, 1 ,[], context=context)
        lst = [
                x.name.lower().strip() for x in self.browse(cr, uid, sr_ids, context=context)
                if x.name and x.id not in ids
              ]
        for self_obj in self.browse(cr, uid, ids, context=context):
            if self_obj.name and self_obj.name.lower() and self_obj.name.strip() and self_obj.name.lower().strip() in  lst:
                return False
        return True

    #_constraints = [(_check_unique_name_insesitive, 'Error: Nama Sudah Tersedia ', ['name'])]
partner_employee_title()

class partner_employee_school(osv.Model):
    _name = "partner.employee.school"
    _description = "Nama Sekolah"
    _columns = {
        'name': fields.char("Nama Sekolah", size=250, required=True),
        'location': fields.char("Lokasi Sekolah", size=150, required=True),
        'school_type': fields.selection([('sma', 'SMA'), ('smk', 'SMK'), ('stm', 'STM'), ('universitas', 'Universitas'), ('politeknik', 'Politeknik'), ('universitas', 'Universitas'), ('sekolah_tinggi', 'Sekolah Tinggi'), ('institut', 'Institut'), ('lainnya', 'Lainnya')], 'Tipe'),
    }
    def _check_unique_name_insesitive(self, cr, uid, ids, context=None):
        sr_ids = self.search(cr, 1 ,[], context=context)
        lst = [
                x.name.lower().strip() for x in self.browse(cr, uid, sr_ids, context=context)
                if x.name and x.id not in ids
              ]
        for self_obj in self.browse(cr, uid, ids, context=context):
            if self_obj.name and self_obj.name.lower() and self_obj.name.strip() and self_obj.name.lower().strip() in  lst:
                return False
        return True

   # _constraints = [(_check_unique_name_insesitive, 'Error: Nama Sudah Tersedia ', ['name'])]
partner_employee_school()
class partner_employee_study(osv.Model):
    _name = "partner.employee.study"
    _description = " Jurusan"
    _columns = {
        'name': fields.char("Jurusan Pendidikan", size=150, required=True),
        'description': fields.text("Keterangan Jurusan"),
    }
    def _check_unique_name_insesitive(self, cr, uid, ids, context=None):
        sr_ids = self.search(cr, 1 ,[], context=context)
        lst = [
                x.name.lower().strip() for x in self.browse(cr, uid, sr_ids, context=context)
                if x.name and x.id not in ids
              ]
        for self_obj in self.browse(cr, uid, ids, context=context):
            if self_obj.name and self_obj.name.lower() and self_obj.name.strip() and self_obj.name.lower().strip() in  lst:
                return False
        return True

   # _constraints = [(_check_unique_name_insesitive, 'Error: Nama Sudah Tersedia ', ['name'])]
    
partner_employee_study()
class partner_employee_study_degree(osv.Model):
    _name = "partner.employee.study.degree"
    _description = "Level Jenjang Pendidikan"
    _columns = {
        'name': fields.char("Level Pendidikan", size=100,required=True ),
        'degree_level': fields.integer("Nilai Level", required=True ),
        'description': fields.text("Deskripsi"),
                
    }
    def _check_unique_name_insesitive(self, cr, uid, ids, context=None):
        sr_ids = self.search(cr, 1 ,[], context=context)
        lst = [
                x.name.lower().strip() for x in self.browse(cr, uid, sr_ids, context=context)
                if x.name and x.id not in ids
              ]
        for self_obj in self.browse(cr, uid, ids, context=context):
            if self_obj.name and self_obj.name.lower() and self_obj.name.strip() and self_obj.name.lower().strip() in  lst:
                return False
        return True

    _constraints = [(_check_unique_name_insesitive, 'Error: Nama Sudah Tersedia ', ['name'])]
partner_employee_study_degree()
class partner_employee_study_type(osv.Model):
    _name = "partner.employee.study.type"
    _description = "Tingkat Pendidikan"
    _columns = {
        'name': fields.char("Tingkat Pendidikan", size=25,required=True ),
        'degree_id': fields.many2one('partner.employee.study.degree', 'Level'),
        'description': fields.text("Deskripsi"),
                
    }
    def _check_unique_name_insesitive(self, cr, uid, ids, context=None):
        sr_ids = self.search(cr, 1 ,[], context=context)
        lst = [
                x.name.lower().strip() for x in self.browse(cr, uid, sr_ids, context=context)
                if x.name and x.id not in ids
              ]
        for self_obj in self.browse(cr, uid, ids, context=context):
            if self_obj.name and self_obj.name.lower() and self_obj.name.strip() and self_obj.name.lower().strip() in  lst:
                return False
        return True

    _constraints = [(_check_unique_name_insesitive, 'Error: Nama Sudah Tersedia ', ['name'])]
partner_employee_study_type()

class partner_employee_diklat_type(osv.Model):
    _name = "diklat.type"
    _description = "Jenjang Jenis Diklat"
    _columns = {
        'name': fields.char("Jenjang Diklat", size=250,required=True ),
        'type': fields.selection([('kepemimpinan', 'Kepemimpinan'), 
                                  ('fungsional', 'Fungsional'), 
                                  ('teknik', 'Teknik'),
                                  ('kompetensi', 'Kompetensi'),], 'Tipe Diklat',required=True),
                
    }
partner_employee_diklat_type()

class partner_employee_bidang_kompetensi_type(osv.Model):
    _name = "bidang.kompetensi.type"
    _description = "Jenis Bidang Kompetensi"
    _columns = {
        'code': fields.char("Kode", size=5,required=True ),
        'name': fields.char("Bidang Kompetensi", size=250,required=True ),
    }
    def _check_unique_name_insesitive(self, cr, uid, ids, context=None):
        sr_ids = self.search(cr, 1 ,[], context=context)
        lst = [
                x.name.lower().strip() for x in self.browse(cr, uid, sr_ids, context=context)
                if x.name and x.id not in ids
              ]
        for self_obj in self.browse(cr, uid, ids, context=context):
            if self_obj.name and self_obj.name.lower() and self_obj.name.strip() and self_obj.name.lower().strip() in  lst:
                return False
        return True

    _constraints = [(_check_unique_name_insesitive, 'Error: Nama Sudah Tersedia ', ['name'])]
partner_employee_bidang_kompetensi_type()

