import datetime
import time
from openerp.osv import osv
from openerp.report import report_sxw

NAMA_BULAN =['FALSE','Januari','Februari','Maret','April','Mei','Juni',
             'Juli','Agustus','September','Oktober','November','Desember']
NAMA_HARI = ['FALSE','Minggu','Senin','Selasa','Rabu','Kamis','Jumat','Sabtu']
class kartu_peserta_object_printout_report_parser(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context):
        super(kartu_peserta_object_printout_report_parser, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'get_date': self._get_date,
            'date_format': self._date_format,
            'time_format': self._time_format,
            'get_param': self._get_param,
            'get_logo': self._get_logo,
        })
    
    def _date_format(self,tgl,type):
        if tgl != False :
        
            if type == 1 :
                _date = datetime.datetime.strptime(tgl, '%Y-%m-%d').strftime('%d')
                _month= int(datetime.datetime.strptime(tgl, '%Y-%m-%d').strftime('%m'))
                _year= datetime.datetime.strptime(tgl, '%Y-%m-%d').strftime('%Y')
                _day = int(datetime.datetime.strptime(tgl, '%Y-%m-%d').isoweekday())
                _day_name = NAMA_HARI[_day]
                date_val = "%s %s %s" % (_date,NAMA_BULAN[_month],_year)
            else :
    
                _date = datetime.datetime.strptime(tgl, '%Y-%m-%d %H:%M:%S').strftime('%d')
                _month= int(datetime.datetime.strptime(tgl, '%Y-%m-%d %H:%M:%S').strftime('%m'))
                _year= datetime.datetime.strptime(tgl, '%Y-%m-%d %H:%M:%S').strftime('%Y')
                _day = int(datetime.datetime.strptime(tgl, '%Y-%m-%d %H:%M:%S').isoweekday())
                _day_name = NAMA_HARI[_day]
                date_val = "%s %s %s" % (_date,NAMA_BULAN[_month],_year)
                date_val = "%s , %s %s %s" % (_day_name,_date,NAMA_BULAN[_month],_year)
        else :
            date_val = ''
        return date_val
        
    def _time_format(self,time):
        if time != False :
            time_val = datetime.datetime.strptime(time, '%Y-%m-%d %H:%M:%S').strftime('%H:%M')
        else :
            time_val = ''
        return time_val
        
    def _get_date(self):
        now = datetime.datetime.now()
        current_year = int(now.strftime("%Y"))
        current_month = int(now.strftime("%m"))
        current_date = now.strftime("%d")
        date_val = " %s %s" % (NAMA_BULAN[current_month],current_year)
        return date_val

    def _get_param(self,index):
        query = """
                select other from konfigurasi_umum_upkp_ud where
                key ='KARTUPESERTA'
                """			
        param = []
        self.cr.execute(query, param)
        result = self.cr.fetchone()[0]
        #print "result : ",result," ",index
        explode_val = result.split('|')
        value =  explode_val[index]
        
        return value

    def _get_logo(self):
        print ".....................get logo........"
        query = """
                select value_img from konfigurasi_umum_upkp_ud where
                key ='LOGO'
                """
        print 'query : ',query
        self.cr.execute(query,[])
        result = self.cr.fetchone()[0]
        print "result........",result
        return result

class kartu_peserta_object_report(osv.AbstractModel):
    _name = 'report.df_upkp_ud_kartu_peserta_report.kartu_peserta_object_printout_report_template'
    _inherit = 'report.abstract_report'
    _template = 'df_upkp_ud_kartu_peserta_report.kartu_peserta_object_printout_report_template'
    _wrapped_report_class = kartu_peserta_object_printout_report_parser