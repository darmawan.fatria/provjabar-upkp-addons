##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2016
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv
from mx import DateTime


# ====================== res partner ================================= #

#-----------------
IMPORT_STATE =[('new', 'new'), ('done', 'done'),('failed', 'failed')]
PROV_JABAR_COMPANY_ID = 3
PROVINSI_JAWA_BARAT='Provinsi Jawa Barat'
class temp_partner_employee_data(osv.Model):
    _name = "temp.partner.employee.data"
    _description = "Temporari data pegawai"


    _columns = {

        'name': fields.char ('NAMA', size=400 ),
        'nip': fields.char ('NIP', size=30 ),
        'notes':fields.text('catatan'),
        'f1': fields.char ('1', size=400 ),
        'f2': fields.char ('2', size=400 ),
        'f3': fields.char ('3', size=400 ),
        'f4': fields.char ('4', size=400 ),
        'f5': fields.char ('5', size=400 ),
        'f6': fields.char ('6', size=400 ),
        'f7': fields.char ('7', size=400 ),
        'f8': fields.char ('8', size=400 ),
        'f9': fields.char ('9', size=400 ),
        'f10': fields.char ('10', size=400 ),
        'f11': fields.char ('11', size=400 ),
        'f12': fields.char ('12', size=400 ),
	    'f13': fields.char ('13', size=400 ),
        'f14': fields.char ('14', size=400 ),
        'f15': fields.char ('15', size=400 ),
        'f16': fields.char ('16', size=400 ),
        'f17': fields.char ('18', size=400 ),
        'f18': fields.char ('18', size=400 ),
        'f19': fields.char ('19', size=400 ),
        'f20': fields.char ('20', size=400 ),
        'f21': fields.char ('21', size=400 ),
        'f22': fields.char ('22', size=400 ),
        'f23': fields.char ('23', size=400 ),
        'f24': fields.char ('24', size=400 ),
        'f25': fields.char ('25', size=400 ),
        'f26': fields.char ('26', size=400 ),
        'f27': fields.char ('27', size=400 ),
        'f28': fields.char ('28', size=400 ),
        'f29': fields.char ('29', size=400 ),
        'f30': fields.char ('30', size=400 ),
        'state': fields.selection(IMPORT_STATE, 'Status',required=False),



    }
    _defaults = {
		'state' : 'new',

	}
    def import_to_res_partner(self,cr,uid,ids,context=None):

        data_ids = self.search(cr,uid,[('state', '=', 'new'),],limit=10000)
        partner_pool = self.pool.get('res.partner')
        print ("len : ",len(data_ids))
        for data in self.browse(cr,uid,data_ids,context=None):
            try :
                employee_data =  {}
                company_id = None
                tmt_cpns=None
                tmt_pns=None
                tanggal_lahir=None
                tmt_golongan=None
                gelar_depan_id=None
                gelar_belakang_id=None
                department_id=None
                nip_lama=None
                tempat_lahir=None
                jenis_kelamin=None
                tahun_lulus = None
                gol_awal_id=None
                gol_akhir_id=None
                job_type = None
                tmt_job=None
                job_id=None
                jenjang_pendidikan_id=None

                date_pattern = '%d - %m - %Y'
                db_date_pattern='%Y-%m-%d'

                nip = data.f2
                nama = data.f4

                nip_lama = data.f3
                tempat_lahir  = data.f7
                jenis_kelamin = data.f12
                tahun_lulus = data.f26

                #COMPANY
                parent_company_name = data.f30
                company_name = data.f24
                if not company_name:
                    company_name = data.f23
                if not company_name :
                    company_name = parent_company_name
                parent_company_id = self.get_company_id(cr,uid,parent_company_name,PROV_JABAR_COMPANY_ID)
                company_id = self.create_and_get_company(cr,uid,company_name,parent_company_name)
                print nip," : ",nama,"  =  ",str(company_id),' : ',company_name
                #UNIT KERJA
                department_name =data.f23
                department_id = self.create_and_get_unit_kerja(cr,uid,department_name,company_id or parent_company_id or PROV_JABAR_COMPANY_ID )
                #JABATAN
                tmt_jft_jfu = data.f20
                jft= data.f21
                jfu=data.f22
                tmt_jabatan_sturktural = data.f18
                jabatan_struktural= data.f19
                if jabatan_struktural :
                    job_type='struktural'
                    tmt_job=tmt_jabatan_sturktural
                    job_id = self.create_and_get_jabatan(cr,uid,jabatan_struktural,job_type)
                elif jft:
                    job_type='jft'
                    tmt_job=tmt_jft_jfu
                    job_id = self.create_and_get_jabatan(cr,uid,jft,job_type)
                else:
                    job_type='jfu'
                    tmt_job=tmt_jft_jfu
                    job_id = self.create_and_get_jabatan(cr,uid,jfu,job_type)
                #GOLONGAN
                gol_awal_name = data.f9
                gol_awal_id = self.create_and_get_golongan(cr,uid,gol_awal_name)
                gol_akhir_name = data.f13
                gol_akhir_id = self.create_and_get_golongan(cr,uid,gol_akhir_name)
                #TMT
                if tmt_job:
                    tmt_job = DateTime.strptime(tmt_job,date_pattern).strftime(db_date_pattern)
                if data.f14:
                    tmt_golongan = DateTime.strptime(data.f14,date_pattern).strftime(db_date_pattern)
                if data.f10:
                    tmt_cpns = DateTime.strptime(data.f10,date_pattern).strftime(db_date_pattern)
                if data.f11 :
                    tmt_pns = DateTime.strptime(data.f11,date_pattern).strftime(db_date_pattern)
                if data.f8 :
                    tanggal_lahir = DateTime.strptime(data.f8,date_pattern).strftime(db_date_pattern)
                #PENDIDIKAN
                jenjang_dan_pendidikan = data.f25
                if jenjang_dan_pendidikan:
                    arr = jenjang_dan_pendidikan.split(" ")
                    if arr:
                        jenjang_pendidikan_name = arr[0]
                        jenjang_pendidikan_id = self.create_and_get_jenjang_pendidikan(cr,uid,jenjang_pendidikan_name)
                        jurusan_name = jenjang_dan_pendidikan.replace(jenjang_pendidikan_name+" ","")
                        jurusan_id = self.create_and_get_jenjang_jurusan(cr,uid,jurusan_name)
                #GELAR
                gelar_depan = data.f5
                gelar_depan_id = self.create_and_get_gelar(cr,uid,gelar_depan,'gelar_depan')
                gelar_belakang =data.f6
                gelar_belakang_id = self.create_and_get_gelar(cr,uid,gelar_belakang,'gelar_belakang')

                #DATA
                employee_data.update({
                        'nip':nip,
                        'name':nama,
                        'company_id':company_id,
                        'nip_lama':nip_lama,
                        'tempat_lahir':tempat_lahir,

                        'golongan_awal_id':gol_awal_id,
                        'jenis_kelamin':jenis_kelamin,
                        'golongan_id':gol_akhir_id,
                        'tmt_golongan':tmt_golongan,

                        'tmt_job':tmt_job,
                        'job_id':job_id,
                        'job_type':job_type,
                        'department_id':department_id,

                        'jurusan_id':jurusan_id,
                        'jenjang_pendidikan_id':jenjang_pendidikan_id,
                        'tahun_lulus':tahun_lulus,

                        'tanggal_lahir':tanggal_lahir,
                        'tmt_cpns':tmt_cpns,
						'tmt_pns':tmt_pns,
						'gelar_depan':gelar_depan_id,
						'gelar_blk':gelar_belakang_id,

				})
                #print "employee_data : ",employee_data
                partner_pool.create(cr,uid,employee_data,context=None)
                self.write(cr,uid,[data.id],{'state':'done'}, context=context)
                cr.commit();
            except:
                cr.commit();
                print "got error....."
                self.write(cr,uid,[data.id,],{'state':'failed'}, context=None)
                continue

    def create_and_get_gelar(self,cr,uid,name,title_type,context=None):
        pool = self.pool.get('partner.employee.title')
        if not name:
            return None
        data_ids = pool.search(cr,uid,[('name', '=', name),('title_type', '=', title_type)])
        if not data_ids:
            data_create =  {}
            data_create.update({
						   'name':name,
							'title_type':title_type,
				})
            new_id = pool.create(cr, uid, data_create,context=None)
            return new_id


        return data_ids[0]
    def get_company_id(self,cr,uid,name,parent_id,context=None):
        pool = self.pool.get('res.company')
        if not name or not parent_id:
            return None
        parent_ids = pool.search(cr,uid,[('name', '=', name),('parent_id', '=', parent_id)])

        if not parent_ids:

            return None


        return parent_ids[0]
    def create_and_get_company(self,cr,uid,name,parent_name,context=None):
        pool = self.pool.get('res.company')
        if not name or not parent_name:
            return None
        parent_ids = pool.search(cr,uid,[('name', '=', parent_name),('parent_id', '=', PROV_JABAR_COMPANY_ID)])
        parent_id =None
        if not parent_ids:
            data_create =  {}
            data_create.update({
						   'name':parent_name,
						   'parent_id':PROV_JABAR_COMPANY_ID,
				})
            parent_id = pool.create(cr, uid, data_create,context=None)
        else :
            parent_id = parent_ids[0]
        full_name = name+" "+parent_name
        data_ids = pool.search(cr,uid,[('name', '=', full_name),('parent_id', '=', parent_id)])
        if not data_ids:
            data_create =  {}
            data_create.update({
						   'name':full_name,
							'parent_id':parent_id,
				})
            new_id = pool.create(cr, uid, data_create,context=None)
            return new_id


        return data_ids[0]
    def create_and_get_unit_kerja(self,cr,uid,name,company_id,context=None):
        pool = self.pool.get('partner.employee.department')
        if not name:
            return None
        data_ids = pool.search(cr,uid,[('name', '=', name),('company_id', '=', company_id)])
        if not data_ids:
            data_create =  {}
            data_create.update({
						   'name':name,
							'company_id':company_id,
				})
            new_id = pool.create(cr, uid, data_create,context=None)
            return new_id


        return data_ids[0]
    def create_and_get_golongan(self,cr,uid,name,context=None):
        pool = self.pool.get('partner.employee.golongan')
        if not name:
            return None
        data_ids = pool.search(cr,uid,[('name', '=', name)])
        if not data_ids:
            data_create =  {}
            data_create.update({
						   'name':name,
                            'level':99,
                            'golongan':99,

				})
            new_id = pool.create(cr, uid, data_create,context=None)
            return new_id


        return data_ids[0]
    def create_and_get_jabatan(self,cr,uid,name,job_type,context=None):
        pool = self.pool.get('partner.employee.job')
        if not name:
            return None
        data_ids = pool.search(cr,uid,[('name', '=', name),('job_type', '=', job_type)])
        if not data_ids:
            data_create =  {}
            data_create.update({
						   'name':name,
							'job_type':job_type,
				})
            new_id = pool.create(cr, uid, data_create,context=None)
            return new_id


        return data_ids[0]
    def create_and_get_jenjang_pendidikan(self,cr,uid,name,context=None):
        pool = self.pool.get('partner.employee.study.type')
        if not name:
            return None
        data_ids = pool.search(cr,uid,[('name', '=', name)])
        if not data_ids:
            data_create =  {}
            data_create.update({
						   'name':name,

				})
            new_id = pool.create(cr, uid, data_create,context=None)
            return new_id


        return data_ids[0]
    def create_and_get_jenjang_jurusan(self,cr,uid,name,context=None):
        pool = self.pool.get('partner.employee.study')
        if not name:
            return None
        data_ids = pool.search(cr,uid,[('name', '=', name)])
        if not data_ids:
            data_create =  {}
            data_create.update({
						   'name':name,

				})
            new_id = pool.create(cr, uid, data_create,context=None)
            return new_id


        return data_ids[0]



temp_partner_employee_data()