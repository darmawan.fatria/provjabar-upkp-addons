##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2016
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

{
    "name": "Import Data Kepegawaian",
    "version": "1.0",
    "author": "Darmawan Fatriananda",
    "category": "Partner/Kepegawaian",
    "description": """
        Import Data Kepegawaian
    """,
    "website" : "http://-",
    "license" : "GPL-3",
    "depends": [
                "df_partner_employee_sapk",
                ],
    'data': [
                   "partner_employee_import_view.xml",

                   
                   ],
    #'demo_xml': [],
    'installable': True,
    #'active': False,
}
