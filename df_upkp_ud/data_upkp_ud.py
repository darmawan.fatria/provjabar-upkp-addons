from openerp.osv import fields, osv
from konfigurasi_upkp_ud import TIPE_UJIAN_LIST


class data_upkp_ud(osv.Model):
    _name = 'data.upkp.ud'
    _description = 'Upload data generate'

    _columns = {
        'nip': fields.char('NIP', size=28, required=True),
        'nama': fields.char('Nama', size=512, required=False),
        'no_ujian': fields.char('No Ujian', size=128, required=True),
        'tempat_ujian': fields.char('Tempat Ujian', size=268, required=True),
        'jadwal_ujian': fields.char('Jadwal Ujian', size=268, required=True),
        'tipe_ujian': fields.selection(TIPE_UJIAN_LIST, 'Tipe Ujian', required=True),
        'tingkat_ujian': fields.char('Tingkat Ujian', size=56, required=True),
        'unit_kerja': fields.char('Unit Kerja', size=268, required=False),
        'notes': fields.text('Catatan', ),
        'state': fields.selection([('new', 'Baru'), ('success', 'Berhasil'), ('failed', 'Gagal')], 'Status',
                                  required=False),
    }
    _defaults = {
        'state': 'new'
    }

    def do_update_pendaftaran(self, cr, uid, ids, context=None):
        pendaftaran_pool = self.pool.get('pendaftaran.upkp.ud');
        for data in self.browse(cr, uid, ids, context=context):
            pendaftaran_ids = pendaftaran_pool.search(cr, uid, [('nip', '=', data.nip), ('state', '=', 'confirm')])
            if pendaftaran_ids:
                pendaftaran_pool.action_generate_data_ujian(cr, uid, pendaftaran_ids, data, context=None)
                self.write(cr, uid, data.id, {'state': 'success'}, context=None)
            else :
                self.write(cr, uid, data.id, {'state': 'failed','notes':'Data tidak ditemukan'}, context=None)


data_upkp_ud()
