from openerp.osv import fields, osv
from openerp.tools.translate import _
import time

class notification_employee_register_upkp_ud(osv.osv_memory):
    _name = "notification.employee.register.upkp.ud"
    _columns = {
        'name': fields.char('Notif', size=128),
    }
notification_employee_register_upkp_ud();

class res_partner(osv.Model):
	 _inherit = 'res.partner'

	 def action_register_ud_popup(self,cr,uid,ids,context=None):
		if not ids: return []
		str_tipe_ujian= 'ud'
		for employee_obj in self.browse(cr, uid, ids, context=context):
			employee_id = employee_obj.id
			is_can_register = False;
			if employee_obj.golongan_id and employee_obj.golongan_id.ud:
				is_can_register = True

			if not employee_obj.golongan_id:
				raise osv.except_osv(_('Proses Pendaftaran Tidak Bisa Dilanjutkan!'),
	                                          _('Mohon maaf, silahkan lengkapi data Pangkat/Golongan di data kepegawaian'))

			if not is_can_register:
				raise osv.except_osv(_('Proses Pendaftaran Tidak Bisa Dilanjutkan!'),
	                                          _('Mohon maaf, pangkat/golongan anda tidak diperbolehkan untuk mengikuti Ujian Dinas.'))

			tipe_ujian_pool = self.pool.get('konfigurasi.tipe.ujian')
			tipe_ujian_ids = tipe_ujian_pool.search(cr, uid, [('tipe_ujian','=',str_tipe_ujian)], context=None)
			if tipe_ujian_ids and len(tipe_ujian_ids) > 0 :
				tipe_ujian_id = tipe_ujian_ids[0]
				now=time.strftime('%Y-%m-%d')
				print "now : ",now
				print "tipe : ",tipe_ujian_id
				periode_pool = self.pool.get('konfigurasi.periode.upkp.ud')
				periode_ids = periode_pool.search(cr, uid, [('tipe_id','=',tipe_ujian_id),
				                                               ('tgl_pendaftaran_awal','<=',now),
				                                               ('tgl_pendaftaran_akhir','>=',now),

				                                               ], context=None)
				print "periode_ids ",periode_ids
				if periode_ids and len(periode_ids) > 0:
					current_year =  time.strftime('%Y')
					last_year = int(current_year) -1
					last_two_year = int(current_year) -2
					if len(periode_ids) == 1:
						pendaftaran_pool = self.pool.get('pendaftaran.upkp.ud')
						parent_company_id = employee_obj.company_id.parent_id and employee_obj.company_id.parent_id.id or None
						pendaftaran_data =  {}
						pendaftaran_data.update({
								   'pegawai_id':employee_id,
									'periode_id':periode_ids[0],
									'nilai_skp_1':0,
									'nilai_skp_2':0,
									'parent_company_id' : parent_company_id,
									'tahun_skp_1':str(last_year),
									'tahun_skp_2':str(last_two_year),
									'tgl_pendaftaran':now,
									'tipe_ujian' : str_tipe_ujian,
									'tingkat_ujian_dinas' : employee_obj.golongan_id and employee_obj.golongan_id.tingkat_ujian_dinas or '',
									'kategori_sarjana' : employee_obj.jenjang_pendidikan_id and employee_obj.jenjang_pendidikan_id.kategori_sarjana or '',
						})
						print "pendaftaran_data : ",pendaftaran_data
						pendaftaran_id = pendaftaran_pool.create(cr, uid, pendaftaran_data,context)


					else :
						print "multi period.."
						dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'df_upkp_ud', 'action_upkp_ud_register_popup_form_view')
						return {
							'name':_("Silahkan pilih periode ujian"),
							'view_mode': 'form',
							'view_id': view_id,
							'view_type': 'form',
							'res_model': 'pendaftaran.upkp.ud.draft',
							'type': 'ir.actions.act_window',
							'nodestroy': True,
							'target': 'new',
							'domain': '[]',
							'context': {
							    'default_periode_ids': periode_ids,
							    'default_tipe_ujian':str_tipe_ujian,
								'default_pegawai_id':employee_id,
								'default_nilai_skp_1':0,
								'default_nilai_skp_2':0,
									'default_tahun_skp_1':str(last_year),
									'default_tahun_skp_2':str(last_two_year),
									'default_tgl_pendaftaran':now,
							}
						}
				else :
					raise osv.except_osv(_('Tidak ada periode pendaftaran!'),
	                                          _('Mohon maaf, untuk saat ini tidak ada jadwal pendaftaran ujian dinas.'))

			else :
				raise osv.except_osv(_('Tidak ada periode pendaftaran!'),
	                                          _('Mohon maaf, untuk saat ini tidak ada jadwal pendaftaran ujian dinas.'))


		return {
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'notification.employee.register.upkp.ud',
                'target': 'new',
                'context': context,
            }
	 def action_register_upkp_popup(self,cr,uid,ids,context=None):
		if not ids: return []
		str_tipe_ujian= 'upkp'
		for employee_obj in self.browse(cr, uid, ids, context=context):
			employee_id = employee_obj.id
			tipe_ujian_pool = self.pool.get('konfigurasi.tipe.ujian')
			tipe_ujian_ids = tipe_ujian_pool.search(cr, uid, [('tipe_ujian','=',str_tipe_ujian)], context=None)
			if tipe_ujian_ids and len(tipe_ujian_ids) > 0 :
				tipe_ujian_id = tipe_ujian_ids[0]
				now=time.strftime('%Y-%m-%d')
				print "now : ",now
				print "tipe : ",tipe_ujian_id
				periode_pool = self.pool.get('konfigurasi.periode.upkp.ud')
				periode_ids = periode_pool.search(cr, uid, [('tipe_id','=',tipe_ujian_id),
				                                               ('tgl_pendaftaran_awal','<=',now),
				                                               ('tgl_pendaftaran_akhir','>=',now),

				                                               ], context=None)
				print "periode_ids ",periode_ids
				if periode_ids and len(periode_ids) > 0:
					current_year =  time.strftime('%Y')
					last_year = int(current_year) -1
					last_two_year = int(current_year) -2
					if len(periode_ids) == 1:

						pendaftaran_pool = self.pool.get('pendaftaran.upkp.ud')
						parent_company_id = employee_obj.company_id.parent_id and employee_obj.company_id.parent_id.id or None
						pendaftaran_data =  {}
						pendaftaran_data.update({
								   'pegawai_id':employee_id,
									'periode_id':periode_ids[0],
									'nilai_skp_1':0,
									'nilai_skp_2':0,
									'parent_company_id':parent_company_id,
									'tahun_skp_1':str(last_year),
									'tahun_skp_2':str(last_two_year),
									'tgl_pendaftaran':now,
									'tipe_ujian' : str_tipe_ujian,
									'tingkat_ujian_dinas' : employee_obj.golongan_id and employee_obj.golongan_id.tingkat_ujian_dinas or '',
									'kategori_sarjana' : employee_obj.jenjang_pendidikan_id and employee_obj.jenjang_pendidikan_id.kategori_sarjana or '',
						})
						print "pendaftaran_data : ",pendaftaran_data
						pendaftaran_id = pendaftaran_pool.create(cr, uid, pendaftaran_data,context)


					else :
						print "multi period.."
						dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'df_upkp_ud', 'action_upkp_ud_register_popup_form_view')
						return {
							'name':_("Silahkan pilih periode ujian"),
							'view_mode': 'form',
							'view_id': view_id,
							'view_type': 'form',
							'res_model': 'pendaftaran.upkp.ud.draft',
							'type': 'ir.actions.act_window',
							'nodestroy': True,
							'target': 'new',
							'domain': '[]',
							'context': {
							    'periode_ids': periode_ids,
							    'default_tipe_ujian':str_tipe_ujian,
								'default_pegawai_id':employee_id,
								'default_nilai_skp_1':0,
								'default_nilai_skp_2':0,
									'default_tahun_skp_1':str(last_year),
									'default_tahun_skp_2':str(last_two_year),
									'default_tgl_pendaftaran':now,
							}
						}
				else :
					raise osv.except_osv(_('Tidak ada periode pendaftaran!'),
	                                          _('Mohon maaf, untuk saat ini tidak ada jadwal pendaftaran upkp.'))

			else :
				raise osv.except_osv(_('Tidak ada periode pendaftaran!'),
	                                          _('Mohon maaf, untuk saat ini tidak ada jadwal pendaftaran upkp.'))


		return {
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'notification.employee.register.upkp.ud',
                'target': 'new',
                'context': context,
            }

res_partner()
TINGKAT_UJIAN_DINAS_STATE =[('Tingkat 1', 'Tingkat 1'), ('Tingkat 2', 'Tingkat 2')]
class partner_employee_golongan(osv.Model):
    _inherit = "partner.employee.golongan"
    _columns = {
        'tingkat_ujian_dinas': fields.selection(TINGKAT_UJIAN_DINAS_STATE,"Tingkat Ujian Dinas", required=False),
	    'ud'                 : fields.boolean("Bisa Daftar Ujian Dinas"),
    }
partner_employee_golongan()

KATEGORI_SARJANA_STATE =[('Sarjana', 'Sarjana'), ('Non Sarjana', 'Non Sarjana')]
class partner_employee_study_type(osv.Model):
    _inherit = "partner.employee.study.type"
    _columns = {
        'kategori_sarjana': fields.selection(KATEGORI_SARJANA_STATE, 'Sarjana/Non Sarjana',required=False),
    }
partner_employee_study_type()
