##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2016
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv
from openerp.tools.translate import _
from konfigurasi_upkp_ud import TIPE_UJIAN_LIST
from partner_employee import KATEGORI_SARJANA_STATE


PENDAFTARAN_UJIAN_STATE =[('draft', 'Draft'), ('verification', 'Verifikasi Provinsi'), ('confirm', 'Pengajuan Diterima'), ('exam', 'Tahap Ujian')
						  , ('remedial', 'Ujian Ulang'), ('graduated', 'Lulus'), ('failed', 'Tidak Lulus')]

_PERSENTASE_HASIL_UJIAN = 0.6
_PERSENTASE_NILAI_SKP  = 0.4
_TIDAK_DINILAI = -1
_PRESENTASE_MINIMAL_LULUS = 60
class pendaftaran_upkp_ud(osv.Model):
	_name = 'pendaftaran.upkp.ud'
	_description ='Periode Ujian'

	def create(self, cr, uid, vals, context=None):
		if vals.get('pegawai_id', False) and  vals.get('periode_id', False):
			pegawai_id = vals.get('pegawai_id', False)
			periode_id = vals.get('periode_id', False)
			data_ids = self.search(cr, uid, [('pegawai_id', '=', pegawai_id),('periode_id','=',periode_id)])
			if data_ids:
				raise osv.except_osv(_('Error!, Proses Tidak Dapat Dilanjutkan!'), _('Pegawai Yang Diajuakan Sudah Terdaftar.'))
		if vals.get('pegawai_id', False) :
			pegawai_id = vals.get('pegawai_id', False)
			employee_obj = self.pool.get('res.partner').browse(cr, uid, pegawai_id, context=context)
			if employee_obj:
				if not employee_obj.golongan_id:
					raise osv.except_osv(_('Proses Pendaftaran Tidak Bisa Dilanjutkan!'),
		                                          _('Mohon maaf, silahkan lengkapi data Pangkat/Golongan di data kepegawaian'))
				if not employee_obj.company_id:
					raise osv.except_osv(_('Proses Pendaftaran Tidak Bisa Dilanjutkan!'),
		                                          _('Mohon maaf, silahkan lengkapi data OPD/Kabupaten/Kota di data kepegawaian'))
				if not employee_obj.job_id:
					raise osv.except_osv(_('Proses Pendaftaran Tidak Bisa Dilanjutkan!'),
		                                          _('Mohon maaf, silahkan lengkapi data Jabatan di data kepegawaian'))
				if not employee_obj.pendidikan_terakhir:
					raise osv.except_osv(_('Proses Pendaftaran Tidak Bisa Dilanjutkan!'),
		                                          _('Mohon maaf, silahkan lengkapi data Pendidikan Terakhir di data kepegawaian'))

				periode_id = vals.get('periode_id', False)
				periode_obj = self.pool.get('konfigurasi.periode.upkp.ud').browse(cr, uid, periode_id, context=context)
				print "periode_obj : ",periode_obj.name
				if periode_obj:
					is_can_register = False;
					if periode_obj.tipe_id and periode_obj.tipe_id.tipe_ujian=='ud'  and employee_obj.golongan_id and employee_obj.golongan_id.ud:
						is_can_register = True
					if periode_obj.tipe_id and periode_obj.tipe_id.tipe_ujian=='ud' and not is_can_register:
						raise osv.except_osv(_('Proses Pendaftaran Tidak Bisa Dilanjutkan!'),
			                                          _('Mohon maaf, pangkat/golongan anda tidak diperbolehkan untuk mengikuti Ujian Dinas'))

		return super(pendaftaran_upkp_ud, self).create(cr, uid, vals, context=context)

	_columns = {
		'name'     : fields.char('Nomor Ujian',size=25),
		'no_surat_lulus'     : fields.char('Nomor Surat Tanda Lulus Ujian',size=25),
		'pegawai_id' : fields.many2one('res.partner', 'Pegawai',domain="[('employee','=',True)]"),
		'parent_company_id' : fields.many2one('res.company', 'Kab/Kota',domain="[('is_main_company','=',True)]"),
		'image': fields.related('pegawai_id', 'image',   type="binary", string='Image', store=True),
		'company_id': fields.related('pegawai_id', 'company_id',   type="many2one", relation='res.company', string='OPD/SKPD', store=True),
		'job_id': fields.related('pegawai_id', 'job_id',   type="many2one", relation='partner.employee.job', string='Jabatan', store=True),
		'department_id': fields.related('pegawai_id', 'department_id',   type="many2one", relation='partner.employee.department', string='Unit Kerja', store=True),
		'golongan_id': fields.related('pegawai_id', 'golongan_id',   type="many2one", relation='partner.employee.golongan', string='Golongan', store=True),
		'tingkat_ujian_dinas': fields.char("Tingkat Ujian Dinas", size=15, required=False),

		'nip': fields.related('pegawai_id', 'nip',   type="char", string='NIP', store=True),
		'tanggal_lahir': fields.related('pegawai_id', 'tanggal_lahir',   type="date", string='Tanggal Lahir', store=True),
		'pendidikan_terakhir': fields.related('pegawai_id', 'pendidikan_terakhir',   type="char",  string='Tingkat Pendidikan', store=True),
		'jenjang_pendidikan_id': fields.related('pegawai_id', 'jenjang_pendidikan_id',  type="many2one",relation='partner.employee.study.type',  string='Tingkat Pendidikan', store=True),
        'kategori_sarjana': fields.selection(KATEGORI_SARJANA_STATE, 'Sarjana/Non Sarjana',required=False),

		'periode_id' : fields.many2one('konfigurasi.periode.upkp.ud', 'Periode'),
		'tipe_id': fields.related('periode_id', 'tipe_id',   type="many2one", relation='konfigurasi.tipe.ujian',string='Tipe Ujian', store=True),
	    'tipe_ujian': fields.selection(TIPE_UJIAN_LIST, 'Tipe Ujian',readonly=True,required=True),

		'catatan' :fields.text("Catatan Tambahan"),
		'catatan_verifikatur': fields.text("Catatan Verifikatur"),
		'tgl_pendaftaran':fields.date('Tanggal Pendaftaran'),

		'nilai_skp_1': fields.float("Nilai SKP/DP3",required=True ),
		'tahun_skp_1': fields.char('Periode Tahun SKP/DP3',size=4,required=True),
		'nilai_skp_2': fields.float("Nilai SKP/DP3",required=True ),
		'tahun_skp_2': fields.char('Periode Tahun SKP/DP3',size=4,required=True),

		'nilai_ujian': fields.float("Nilai Ujian", required=False),
		'persentase_nilai_ujian': fields.float("Persentase Nilai Ujian",required=False ),
		'persentase_nilai_skp': fields.float("Persentase Nilai Ujian", required=False),
		'nilai_total_kelulusan': fields.float("Nilai Total", required=False),

		'no_surat_usulan': fields.char('No Surat Usulan', size=128, ),
		'tgl_surat_usulan': fields.date('Tanggal Surat Usulan'),

		'tempat_ujian_id' : fields.many2one('konfigurasi.tempat.ujian', 'Tempat Ujian', ),
		'jadwal_ujian_id' : fields.many2one('konfigurasi.jadwal.ujian', 'Jadwal Ujian',),
		'no_ujian': fields.char('No Ujian', size=128, required=False),
		'tempat_ujian': fields.char('Tempat Ujian', size=268, required=False),
		'jadwal_ujian': fields.char('Jadwal Ujian', size=268, required=False),

		 'kepgub_pengumuman_kelulusan'     : fields.char('Nomor Kep Gub',size=25),
		 'kepgub_hasil'     : fields.char('Kep Gub Hasil',size=25),


		'state': fields.selection(PENDAFTARAN_UJIAN_STATE, 'Status',required=True),
		'hasil_ids': fields.one2many('hasil.materi.ujian', 'pendaftaran_id', 'Hasill'),

		#dokumen
		'file_sk_golongan_id'            : fields.many2one('ir.attachment','SK Pangkat Terakhir',domain="[('partner_id','=',pegawai_id)]",  ),
		'file_ijazah_pendidikan_id'      : fields.many2one('ir.attachment','Ijazah Terakhir',domain="[('partner_id','=',pegawai_id)]",),
		'file_sk_jabatan_terakhir_id'    : fields.many2one('ir.attachment','SK Jabatan Terakhir',domain="[('partner_id','=',pegawai_id)]"),
		'file_skp_1_id'                  : fields.many2one('ir.attachment','File SKP 1 Tahun Terakhir',domain="[('partner_id','=',pegawai_id)]"),
		'file_skp_2_id'                  : fields.many2one('ir.attachment','File SKP 2 Tahun Terakhir',domain="[('partner_id','=',pegawai_id)]"),
		'file_surat_izin_belajar_id': fields.many2one('ir.attachment','File Surat Izin Belajar',domain="[('partner_id','=',pegawai_id)]"),

		'binary_file_sk_golongan_id': fields.related('file_sk_golongan_id', 'datas', type="binary", string='Pangkat', store=False),
		'binary_file_ijazah_pendidikan_id': fields.related('file_ijazah_pendidikan_id', 'datas', type="binary",
													 string='Ijazah', store=False),
		'binary_file_sk_jabatan_terakhir_id': fields.related('file_sk_jabatan_terakhir_id', 'datas', type="binary",
													 string='Jabatan Terakhir', store=False),
		'binary_file_skp_1_id': fields.related('file_skp_1_id', 'datas', type="binary",
													 string='SKP 1', store=False),
		'binary_file_skp_2_id': fields.related('file_skp_2_id', 'datas', type="binary",
													 string='SKP 2', store=False),
		'binary_file_surat_izin_belajar_id': fields.related('file_surat_izin_belajar_id', 'datas', type="binary",
													 string='Izin Belajar', store=False),

		'user_id_admin' : fields.many2one('res.users', 'Admin'),
		'user_id_verifikatur' : fields.many2one('res.users', 'Verifikatur'),
		'active' : fields.boolean('Aktif'),
		'koreksi': fields.boolean('Ada Koreksi'),

	}
	_defaults = {
		'active' : True,
		'koreksi':False,
		'state' : 'draft',
		'user_id_admin': lambda self,cr,uid,c: self.pool.get('res.users').browse(cr, uid, uid, c).company_id.user_id_admin.id,
		'user_id_verifikatur': lambda self,cr,uid,c: self.pool.get('res.users').browse(cr, uid, uid, c).company_id.user_id_verifikatur.id,

	}
	_order ='tgl_pendaftaran desc'

	_sql_constraints = [
         ('data_unique', 'unique (pegawai_id,periode_id)',
             'Pegawai Sudah Terdaftar Dalam Periode Ini')
     ]

	def action_propose_verification(self, cr, uid, ids, context=None):
		return self.write(cr, uid, ids, {'state':'verification'}, context=context)
	def action_confirm(self, cr, uid, ids, context=None):
		print "confirm..."
		self.write(cr, uid, ids, {'state':'confirm'}, context=context)
		return True
	def action_confirm_popup(self,cr,uid,ids,context=None):
		if not ids: return []
		print "ids : ",ids
		dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'df_upkp_ud', 'action_upkp_ud_confirm_popup_form_view')
		upkp_obj = self.browse(cr, uid, ids[0], context=context)
		is_single_data = False
		if upkp_obj.periode_id and upkp_obj.periode_id.jadwal_ids :
			len_jadwal = len(upkp_obj.periode_id.jadwal_ids)
			is_single_data = (len_jadwal==1)
		if is_single_data and upkp_obj.periode_id and upkp_obj.periode_id.tempat_ids :
			len_tempat = len(upkp_obj.periode_id.tempat_ids)
			is_single_data = (len_tempat==1)
		if is_single_data :
					print(upkp_obj.periode_id.jadwal_ids)
					print(upkp_obj.periode_id.tempat_ids)
					vals = {}
					vals.update({
										'jadwal_ujian_id'       : upkp_obj.periode_id.jadwal_ids[0].jadwal_ujian_id.id,
										'tempat_ujian_id'       : upkp_obj.periode_id.tempat_ids[0].tempat_ujian_id.id,
										'state':'confirm'
					})
					return self.write(cr, uid, upkp_obj.id, vals, context)
		else :
			param_jadwal_ujian_ids = []
			param_tempat_ujian_ids = []
			if upkp_obj.periode_id:
				for jadwal in upkp_obj.periode_id.jadwal_ids:
					param_jadwal_ujian_ids.append(jadwal.jadwal_ujian_id.id)
				for tempat in upkp_obj.periode_id.tempat_ids:
					param_tempat_ujian_ids.append(tempat.tempat_ujian_id.id)

		return {
				'name':_("Terima Pengajuan Pendaftaran UPKP Dan Ujian Dinas"),
				'view_mode': 'form',
				'view_id': view_id,
				'view_type': 'form',
				'res_model': 'pendaftaran.upkp.ud.confirm',
				'type': 'ir.actions.act_window',
				'nodestroy': True,
				'target': 'new',
				'domain': '[]',
				'context': {
					'param_tempat_ujian_ids':param_tempat_ujian_ids,
				    'param_jadwal_ujian_ids':param_jadwal_ujian_ids,
					'default_pendaftaran_upkp_ud_id':upkp_obj.id,
				}
			}

	def action_exam(self, cr, uid, ids, context=None):
		#hasil_ujian_pool = self.pool.get('hasil.materi.ujian')
		#for upkp_ud_obj in self.browse(cr, uid, ids, context=context):
		#	for materi in upkp_ud_obj.periode_id.materi_ids:
		#		hasil =  {}
		#		hasil.update({
		#				   'pendaftaran_id':upkp_ud_obj.id,
		#					'materi_id':materi.materi_id.id,
		#					'nilai':0
		#		})
		#		hasil_materi_id = hasil_ujian_pool.create(cr, uid, hasil,context)
		return self.write(cr, uid, ids, {'state':'exam'}, context=context)
	def action_remedial(self, cr, uid, ids, context=None):
		return self.write(cr, uid, ids, {'state':'remedial'}, context=context)
	def action_reject(self, cr, uid, ids, context=None):
		return self.write(cr, uid, ids, {'state':'draft'}, context=context)
	def action_graduated(self, cr, uid, ids, context=None):
		#updated_data = {
		#                        'nilai_akhir': nilai_akhir,
		#                        'indeks_nilai': self.get_value_poin(cr, uid, ids, nilai_akhir),
		#                        'jumlah_perhitungan':jumlah_perhitungan,
		#                         }
		return self.write(cr, uid, ids, {'state':'graduated'}, context=context)
	def action_failed(self, cr, uid, ids, context=None):
		return self.write(cr, uid, ids, {'state':'failed'}, context=context)
	def do_generate_number(self, cr, uid, ids, context=None):
		seq_pool = self.pool.get('ir.sequence')
		for upkp_ud_obj in self.browse(cr, uid, ids, context=context):
			no_ujian_seq = '-';
			if upkp_ud_obj.tipe_ujian=='upkp':
				no_ujian_seq = seq_pool.next_by_code(cr, uid, 'no.ujian.upkp', context=context)
			elif upkp_ud_obj.tipe_ujian=='ud' and upkp_ud_obj.tingkat_ujian_dinas == 'Tingkat 1':
				no_ujian_seq = seq_pool.next_by_code(cr, uid, 'no.ujian.ud1', context=context)
			elif upkp_ud_obj.tipe_ujian=='ud' and upkp_ud_obj.tingkat_ujian_dinas == 'Tingkat 2':
				no_ujian_seq = seq_pool.next_by_code(cr, uid, 'no.ujian.ud2', context=context)

			print " no ujian : ",no_ujian_seq
			no_ujian_seq= no_ujian_seq.replace('MM_UJIAN',upkp_ud_obj.periode_id.periode_bulan_ujian)
			no_ujian_seq= no_ujian_seq.replace('YYYY_UJIAN',upkp_ud_obj.periode_id.periode_tahun_ujian)
			self.write(cr, uid, [upkp_ud_obj.id], {'name':no_ujian_seq}, context=context)
		return True

	def action_generate_data_ujian(self, cr, uid, ids,data, context=None):
		return self.write(cr, uid, ids, {'name': data.no_ujian,
										 'tempat_ujian': data.tempat_ujian,
										 'jadwal_ujian': data.jadwal_ujian}, context=context)

	def action_generate_hasil_ujian(self, cr, uid, ids, data, context=None):
		#logic
		hasil_ujian_pool = self.pool.get('hasil.materi.ujian')
		konfigurasi_materi_pool = self.pool.get('konfigurasi.materi.ujian')

		for upkp_ud_obj in self.browse(cr, uid, ids, context=context):
			old_hasil_ids = hasil_ujian_pool.search(cr, uid, [('pendaftaran_id', '=', upkp_ud_obj.id)])
			hasil_ujian_pool.unlink(cr,uid,old_hasil_ids,context=None);
			materi_ids = konfigurasi_materi_pool.search(cr,uid,[])
			for materi_obj in konfigurasi_materi_pool.browse(cr,uid,materi_ids,context=None):
				hasil = {}
				if materi_obj.code == 'personal' and data.nilai_personal != _TIDAK_DINILAI:
					hasil.update({
								'pendaftaran_id':upkp_ud_obj.id,
								'materi_id':materi_obj.id,
								'nilai':data.nilai_personal
							})
					hasil_materi_id = hasil_ujian_pool.create(cr, uid, hasil, context)
				if materi_obj.code == 'thinking' and data.nilai_thinking != _TIDAK_DINILAI:
					hasil.update({
						'pendaftaran_id': upkp_ud_obj.id,
						'materi_id': materi_obj.id,
						'nilai': data.nilai_thinking
					})
					hasil_materi_id = hasil_ujian_pool.create(cr, uid, hasil, context)
				if materi_obj.code == 'social' and data.nilai_social != _TIDAK_DINILAI:
					hasil.update({
						'pendaftaran_id': upkp_ud_obj.id,
						'materi_id': materi_obj.id,
						'nilai': data.nilai_social
					})
					hasil_materi_id = hasil_ujian_pool.create(cr, uid, hasil, context)
				if materi_obj.code == 'academic' and data.nilai_academic != _TIDAK_DINILAI:
					hasil.update({
						'pendaftaran_id': upkp_ud_obj.id,
						'materi_id': materi_obj.id,
						'nilai': data.nilai_academic
					})
					hasil_materi_id = hasil_ujian_pool.create(cr, uid, hasil, context)
				if materi_obj.code == 'vocational' and data.nilai_vocational != _TIDAK_DINILAI:
					hasil.update({
						'pendaftaran_id': upkp_ud_obj.id,
						'materi_id': materi_obj.id,
						'nilai': data.nilai_vocational
					})
					hasil_materi_id = hasil_ujian_pool.create(cr, uid, hasil, context)
				if materi_obj.code == 'inggris' and data.nilai_bahasa_inggris != _TIDAK_DINILAI:
					hasil.update({
						'pendaftaran_id': upkp_ud_obj.id,
						'materi_id': materi_obj.id,
						'nilai': data.nilai_bahasa_inggris
					})
					hasil_materi_id = hasil_ujian_pool.create(cr, uid, hasil, context)
				if materi_obj.code == 'tulis' and data.nilai_karya_tulis != _TIDAK_DINILAI:
					hasil.update({
						'pendaftaran_id': upkp_ud_obj.id,
						'materi_id': materi_obj.id,
						'nilai': data.nilai_karya_tulis
					})
					hasil_materi_id = hasil_ujian_pool.create(cr, uid, hasil, context)
			#lulus atau tidak lulus
			nilai_ujian = data.nilai_total
			persentase_nilai_ujian = data.nilai_total * _PERSENTASE_HASIL_UJIAN
			rata_rata_skp = ( upkp_ud_obj.nilai_skp_1 + upkp_ud_obj.nilai_skp_2 )/2
			persentase_nilai_skp = _PERSENTASE_NILAI_SKP * rata_rata_skp
			nilai_total_kelulusan = persentase_nilai_ujian + persentase_nilai_skp
			state_lulus = 'failed'
			if nilai_total_kelulusan > _PRESENTASE_MINIMAL_LULUS:
				state_lulus = 'graduated'

			self.write(cr, uid, ids, {'state': state_lulus,
									  'nilai_ujian': nilai_ujian,
									  'persentase_nilai_ujian': persentase_nilai_ujian,
									  'nilai_total_kelulusan': nilai_total_kelulusan,
									  'persentase_nilai_skp': persentase_nilai_skp}, context=context)

		return True;

	def action_reject_popup(self, cr, uid, ids, context=None):
		if not ids: return []
		print "tolak bkd"
		dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'df_upkp_ud',
																			 'action_upkp_ud_reject_verification_popup_form_view')
		upkp_obj = self.browse(cr, uid, ids[0], context=context)

		return {
			'name': _("Tolak Pengajuan"),
			'view_mode': 'form',
			'view_id': view_id,
			'view_type': 'form',
			'res_model': 'pendaftaran.upkp.ud.reject.verification',
			'type': 'ir.actions.act_window',
			'nodestroy': True,
			'target': 'new',
			'domain': '[]',
			'context': {
				'default_pendaftaran_upkp_ud_id': upkp_obj.id,
			}
		}


pendaftaran_upkp_ud()


class hasil_materi_ujian(osv.Model):
	_name = 'hasil.materi.ujian'
	_description ='Hasil Ujian'

	_columns = {
		'materi_id' : fields.many2one('konfigurasi.materi.ujian', 'Materi Ujian', required=True),
		'nilai': fields.float("Nilai",required=True ),
		'pendaftaran_id' : fields.many2one('pendaftaran.upkp.ud', 'Pendaftaran', ),
	}
hasil_materi_ujian()