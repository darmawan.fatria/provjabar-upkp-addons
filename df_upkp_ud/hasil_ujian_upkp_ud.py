from openerp.osv import fields, osv
from konfigurasi_upkp_ud import TIPE_UJIAN_LIST


class hasil_ujian_upkp_ud(osv.Model):
    _name = 'hasil.ujian.upkp.ud'
    _description = 'Upload hasil ujian generate'

    _columns = {
        'nip': fields.char('NIP', size=28, required=False),
        'nama': fields.char('Nama', size=512, required=False),
        'no_ujian': fields.char('No Ujian', size=128, required=True),
        'tipe_ujian': fields.selection(TIPE_UJIAN_LIST, 'Tipe Ujian', required=True),
        'tingkat_ujian': fields.char('Tingkat Ujian', size=56, required=True),
        'unit_kerja': fields.char('Unit Kerja', size=268, required=False),
        'nilai_personal': fields.float('Personal Skill',),
        'nilai_thinking': fields.float('Thinking Skill', ),
        'nilai_social': fields.float('Social Skill', ),
        'nilai_academic': fields.float('Academic Skill', ),
        'nilai_vocational': fields.float('Vocational Skill', ),
        'nilai_bahasa_inggris': fields.float('Bahasa Inggris', ),
        'nilai_karya_tulis': fields.float('Karya Tulis', ),
        'nilai_total': fields.float('Nilai Total', ),
        'notes': fields.text('Catatan', ),
        'state': fields.selection([('new', 'Baru'), ('success', 'Berhasil'), ('failed', 'Gagal')], 'Status',
                                  required=False),
    }
    _defaults = {
        'state': 'new'
    }
    #Personal Skill
    #Thinking Skill
    #Social
    #Academic
    #Vocational
    #Bahasa Inggris
    #Wawancara
    #Karya Tulis


    def do_update_pendaftaran(self, cr, uid, ids, context=None):
        pendaftaran_pool = self.pool.get('pendaftaran.upkp.ud');
        for data in self.browse(cr, uid, ids, context=context):
            pendaftaran_ids = pendaftaran_pool.search(cr, uid, [('name', '=', data.no_ujian), ('state', 'in', ('exam','remedial'))])
            if pendaftaran_ids:
                pendaftaran_pool.action_generate_hasil_ujian(cr, uid, pendaftaran_ids, data, context=None)
                self.write(cr, uid, data.id, {'state': 'success'}, context=None)
            else :
                self.write(cr, uid, data.id, {'state': 'failed','notes':'Data tidak ditemukan'}, context=None)


hasil_ujian_upkp_ud()
