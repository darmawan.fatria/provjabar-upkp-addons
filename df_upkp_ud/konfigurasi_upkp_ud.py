##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2016
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv

TIPE_UJIAN_LIST =[('upkp', 'Ujian Penyesuaian Kenaikan Pangkat'), ('ud', 'Ujian Dinas')]

class konfigurasi_tipe_ujian(osv.Model):
    _name = 'konfigurasi.tipe.ujian'
    _description ='Tipe Ujian'

    _columns = {
        'name'     : fields.char('Nama',size=50,required=True),
	    'tipe_ujian': fields.selection(TIPE_UJIAN_LIST, 'Tipe Ujian',required=False),
    }

konfigurasi_tipe_ujian()

class konfigurasi_materi_ujian(osv.Model):
    _name = 'konfigurasi.materi.ujian'
    _description ='Materi Ujian'

    _columns = {
        'name'     : fields.char('Nama',size=50,required=True),
        'code': fields.char('Kode', size=50, required=True),
	    'tipe_ujian': fields.selection(TIPE_UJIAN_LIST, 'Tipe Ujian',required=False),
    }

konfigurasi_materi_ujian()
class konfigurasi_umum_upkp_ud(osv.Model):
    _name = 'konfigurasi.umum.upkp.ud'
    _description ='Materi Ujian'

    _columns = {
        'key'     : fields.char('Key',size=15,required=True),
        'name'     : fields.char('Value',size=50,required=True),
        'other'     : fields.text('Other Value'),
        'value_img' : fields.binary('Gambar')

    }

konfigurasi_umum_upkp_ud()