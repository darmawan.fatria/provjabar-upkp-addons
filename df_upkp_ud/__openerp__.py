##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2016
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

{
    "name": "Ujian Dinas Dan Penyesuaian Kenaikan Pangkat",
    "version": "1.0",
    "author": "Darmawan Fatriananda",
    "category": "Kepegawaian/Kenaikan Pangkat",
    "description": """
PENETAPAN KELULUSAN UJIAN DINAS DAN UJIAN PENYESUAIAN KENAIKAN PANGKAT PEGAWAI NEGERI SIPIL    """,
    "website" : "http://-",
    "license" : "GPL-3",
    "depends": [
                "df_partner_employee_sapk",
                ],
    'data': [
                    "security/upkp_ud_security.xml",
                    'security/ir.model.access.csv',
                    "sequence/upkp_ud_data.xml",
                    "konfigurasi_periode_upkp_ud_view.xml",
                    "pendaftaran_upkp_ud_view.xml",
                    "popup/pendaftaran_upkp_ud_confirm_view.xml",
                    "konfigurasi_upkp_ud_view.xml",
                    "partner_employee_view.xml",
                     "company_view.xml",
                    "popup/pendaftaran_upkp_ud_draft_view.xml",
                    "popup/massive_pendaftaran_upkp_ud_process_view.xml",
                    "app_upkp_ud_settings_view.xml",
                    "data_upkp_ud_view.xml",
                    "hasil_ujian_upkp_ud_view.xml",
                    "popup/pendaftaran_upkp_ud_reject_verification_view.xml",
                   ],
    #'demo_xml': [],
    'installable': True,
    #'active': False,
}
