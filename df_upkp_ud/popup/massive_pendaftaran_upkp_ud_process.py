from openerp.osv import fields, osv
from openerp import pooler



class massive_pendaftaran_upkp_process(osv.osv_memory):
    _name = 'massive.pendaftaran.upkp.process'
    _description ='Pendaftaran UPKP Secara Massal'

    def action_register_employees(self, cr, uid,ids, context=None):
        """  Pengajuan pendaftaran Secara massal
        """
        if context is None :
	        context={}
        pool_obj = pooler.get_pool(cr.dbname)
        employee_pool = pool_obj.get('res.partner')
        employee_ids  = context['active_ids']
        return employee_pool.action_register_upkp_popup(cr,uid,employee_ids,context=None)





massive_pendaftaran_upkp_process()
class massive_pendaftaran_ud_process(osv.osv_memory):
    _name = 'massive.pendaftaran.ud.process'
    _description ='Pendaftaran UD Secara Massal'

    def action_register_employees(self, cr, uid,ids, context=None):
        """  Pengajuan pendaftaran Secara massal
        """
        if context is None :
	        context={}
        pool_obj = pooler.get_pool(cr.dbname)
        employee_pool = pool_obj.get('res.partner')
        employee_ids  = context['active_ids']
        return employee_pool.action_register_ud_popup(cr,uid,employee_ids,context=None)
massive_pendaftaran_ud_process()