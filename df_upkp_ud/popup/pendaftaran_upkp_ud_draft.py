##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2016
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv
from openerp.addons.df_upkp_ud.konfigurasi_upkp_ud import TIPE_UJIAN_LIST

class pendaftaran_upkp_ud_draft(osv.Model):
    _name = 'pendaftaran.upkp.ud.draft'
    _description ='Popup Pengajuan Pendaftaran'
    _columns = {
		'pegawai_id' : fields.many2one('res.partner', 'Pegawai',domain="[('employee','=',True)]"),
	    'tipe_ujian': fields.selection(TIPE_UJIAN_LIST, 'Tipe Ujian',required=False),
   		'tgl_pendaftaran':fields.date('Tanggal Pendaftaran'),
	    'nilai_skp_1': fields.float("Nilai SKP/DP3",required=False ),
		'tahun_skp_1': fields.char('Periode Tahun SKP/DP3',size=4,required=False),
		'nilai_skp_2': fields.float("Nilai SKP/DP3",required=False ),
		'tahun_skp_2': fields.char('Periode Tahun SKP/DP3',size=4,required=False),
	    'periode_id' : fields.many2one('konfigurasi.periode.upkp.ud', 'Periode'),


   	}
    def action_register_to_draft(self, cr, uid, ids, context=None):
        """  Pengajuan pendaftaran
        """
        pendaftaran_data = {}
        pendaftaran_pool = self.pool.get('pendaftaran.upkp.ud')
        popup_obj = self.browse(cr, uid, ids[0], context=context)
        tipe_ujian = popup_obj.periode_id and popup_obj.periode_id.tipe_id and popup_obj.periode_id.tipe_id.tipe_ujian or None
        pendaftaran_data.update({
                                'pegawai_id':popup_obj.pegawai_id.id,
								'periode_id':popup_obj.periode_id.id,
								'nilai_skp_1':popup_obj.nilai_skp_1,
								'nilai_skp_2':popup_obj.nilai_skp_2,
								'tahun_skp_1':popup_obj.tahun_skp_1,
								'tahun_skp_2':popup_obj.tahun_skp_2,
								'tgl_pendaftaran':popup_obj.tgl_pendaftaran,
	                            'tipe_ujian' : tipe_ujian
        })
        return pendaftaran_pool.create(cr, uid, pendaftaran_data,context)

pendaftaran_upkp_ud_draft()