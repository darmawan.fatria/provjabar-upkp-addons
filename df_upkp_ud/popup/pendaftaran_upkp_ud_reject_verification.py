##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2016
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv

class pendaftaran_upkp_ud_confirm(osv.Model):
    _name = 'pendaftaran.upkp.ud.reject.verification'
    _description ='Popup Tolak Pengajuan Pendaftaran'

    _columns = {
		'pendaftaran_upkp_ud_id' : fields.many2one('pendaftaran.upkp.ud','Informasi Pendaftaran' ),
   	    'catatan_verifikatur' : fields.text('Catatan'),
    }

    def action_reject_verification(self, cr, uid, ids, context=None):
        """  Pengajuan Diterima
        """
        vals = {}
        pendaftaran_pool = self.pool.get('pendaftaran.upkp.ud')
        popup_obj = self.browse(cr, uid, ids[0], context=context)
        vals.update({
	                            'catatan_verifikatur'       : popup_obj.catatan_verifikatur,
                                'koreksi'   :True,
	                            'state':'draft'
        })
        return pendaftaran_pool.write(cr, uid, popup_obj.pendaftaran_upkp_ud_id.id, vals, context)

pendaftaran_upkp_ud_confirm()