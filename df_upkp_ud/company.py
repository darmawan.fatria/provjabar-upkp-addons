from openerp.osv import fields, osv
from openerp.tools.translate import _

class res_company(osv.Model):
	 _inherit = 'res.company'

	 _columns = {
        'is_main_company'     : fields.boolean('Instansi Induk'),
		'user_id_admin' : fields.many2one('res.users', 'Admin'),
		'user_id_verifikatur' : fields.many2one('res.users', 'Verifikatur'),
	 }
	 _defaults = {
        'is_main_company' : False,

    }

res_company()