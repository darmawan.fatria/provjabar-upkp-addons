from openerp.osv import fields, osv
from string import split

class commons_notification(osv.osv_memory):
	_name = "commons.notification"
	_columns = {
		'name': fields.char('Messages', size=128),
	}
commons_notification();
class app_upkp_ud_settings(osv.osv_memory):
	_name = 'app.upkp.ud.settings'
	_description = 'Konfigurasi Aplikasi SKP'

	_columns = {
			'name' : fields.char('Konfigurasi UPKP UD', size=64, readonly=True),

	}

	# generate no dan jadwal ujian dari hasil upload data
	def generate_no_jadwal_ujian(self, cr, uid, ids, context=None):
		data_pool = self.pool.get('data.upkp.ud')
		data_ids = data_pool.search(cr, uid, [('state', '=', 'new')], )
		if data_ids and len(data_ids) > 0:
			print "len(data_ids) ", len(data_ids)
			data_pool.do_update_pendaftaran(cr, uid, data_ids, context=None)
			print "done..."

		return True

	def generate_hasil_ujian(self, cr, uid, ids, context=None):
		data_pool = self.pool.get('hasil.ujian.upkp.ud')
		data_ids = data_pool.search(cr, uid, [('state', '=', 'new')], )
		if data_ids and len(data_ids) > 0:
			print "len(data_ids) ", len(data_ids)
			data_pool.do_update_pendaftaran(cr, uid, data_ids, context=None)
			print "done..."

	def generate_no_ujian_upkp(self, cr, uid, ids, context=None):
		self.generate_no_ujian(cr,uid,'upkp',context=None)
	def action_tahap_ujian_upkp(self, cr, uid, ids, context=None):
		self.action_tahap_ujian(cr,uid,'upkp',context=None)
	def generate_ruang_ujian_upkp(self, cr, uid, ids, context=None):
		self.generate_ruang_ujian(cr,uid,'upkp',context=None)

	def generate_no_ujian_ud(self, cr, uid, ids, context=None):
		self.generate_no_ujian(cr,uid,'ud',context=None)
	def action_tahap_ujian_ud(self, cr, uid, ids, context=None):
		self.action_tahap_ujian(cr,uid,'ud',context=None)
	def generate_ruang_ujian_ud(self, cr, uid, ids, context=None):
		self.generate_ruang_ujian(cr,uid,'ud',context=None)

	def generate_no_ujian(self, cr, uid, tipe_ujian, context=None):
		pendaftaran_pool = self.pool.get('pendaftaran.upkp.ud')
		pendaftar_ids = pendaftaran_pool.search(cr, uid, [('tipe_ujian', '=', tipe_ujian),
				                                            ('state','=','confirm') ,
				                                            ('name','=',False)],)
		if pendaftar_ids and len(pendaftar_ids) > 0:
			print "len(pendaftar_ids) ",len(pendaftar_ids)
			pendaftaran_pool.do_generate_number(cr,uid,pendaftar_ids,context=None)
			print "done..."

	def action_tahap_ujian(self, cr, uid, tipe_ujian, context=None):
		pendaftaran_pool = self.pool.get('pendaftaran.upkp.ud')
		pendaftar_ids = pendaftaran_pool.search(cr, uid, [('tipe_ujian', '=', tipe_ujian),
				                                            ('state','=','confirm') ,
				                                            ('name','!=',False)],)
		if pendaftar_ids and len(pendaftar_ids) > 0:
			print "len(pendaftar_ids) ",len(pendaftar_ids)
			pendaftaran_pool.action_exam(cr,uid,pendaftar_ids,context=None)
			print "done..."
	def generate_ruang_ujian(self, cr, uid, tipe_ujian, context=None):
		periode_pool = self.pool.get('konfigurasi.periode.upkp.ud')
		tipe_pool = self.pool.get('konfigurasi.tipe.ujian')
		tipe_ids = tipe_pool.search(cr,uid,[('tipe_ujian', '=', tipe_ujian)],context=None)
		periode_ids = periode_pool.search(cr, uid, [('tipe_id', 'in', tipe_ids),
				                                            ('aktif','=',True)],)
		if periode_ids and len(periode_ids) > 0:
			print "len(periode_ids) ",len(periode_ids)
			if tipe_ujian == 'upkp' :
				periode_pool.action_generate_venue_by_type(cr,uid,periode_ids,'Sarjana',context=None)
				print "done..."
				periode_pool.action_generate_venue_by_type(cr,uid,periode_ids,'Non Sarjana',context=None)
				print "done..."
			else :
				periode_pool.action_generate_venue(cr,uid,periode_ids,context=None)
				print "done..."


app_upkp_ud_settings()