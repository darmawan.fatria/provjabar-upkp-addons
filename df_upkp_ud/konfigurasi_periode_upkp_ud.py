##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2016
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

from openerp.osv import fields, osv
from partner_employee import KATEGORI_SARJANA_STATE


PERIODE_BULAN_LIST =[('01', 'Januari'), ('02', 'Pebruari'),
													  ('03', 'Maret'), ('04', 'April'),
													  ('05', 'Mei'), ('06', 'Juni'),
													  ('07', 'Juli'), ('08', 'Agustus'),
													  ('09', 'September'), ('10', 'Oktober'),
													  ('11', 'November'), ('12', 'Desember')]

class konfigurasi_periode_upkp_ud(osv.Model):
	_name = 'konfigurasi.periode.upkp.ud'
	_description ='Periode Ujian'

	_columns = {
		'name'     : fields.char('Nama',size=50,required=True),
		'periode_tahun'     : fields.char('Periode Tahun',size=4,required=True),
		'periode_bulan': fields.selection(PERIODE_BULAN_LIST, 'Periode Bulan',required=True),
		'periode_tahun_ujian'     : fields.char('Periode Tahun Ujian',size=4,required=True),
		'periode_bulan_ujian': fields.selection(PERIODE_BULAN_LIST, 'Periode Bulan Ujian',required=True),
		'tipe_id' : fields.many2one('konfigurasi.tipe.ujian', 'Tipe Ujian', ),
		'catatan' :fields.text("Catatan Tambahan"),

		'tgl_pendaftaran_awal':fields.date('Periode Awal Pendaftaran Ujian'),
		'tgl_pendaftaran_akhir':fields.date('Periode Akhir Pendaftaran Ujian '),
		'tgl_awal_ujian':fields.date('Periode Awal Tanggal  Ujian'),
		'tgl_akhir_ujian':fields.date('Periode Akhir Tanggal Ujian'),

		'jadwal_ids': fields.one2many('konfigurasi.periode.jadwal.ujian', 'periode_id', 'Daftar Jadwal'),
		'tempat_ids': fields.one2many('konfigurasi.periode.tempat.ujian', 'periode_id', 'Daftar Tempat'),
		'materi_ids': fields.one2many('konfigurasi.periode.materi.ujian', 'periode_id', 'Materi Ujian'),
		'peserta_ids': fields.one2many('konfigurasi.periode.peserta.ujian', 'periode_id', 'Peserta Ujian'),

		'aktif' : fields.boolean('Aktif'),




	}

	def action_generate_venue(self, cr, uid, ids, context=None):
		print "action_generate_venue..."
		ruang_peserta_pool = self.pool.get('konfigurasi.periode.peserta.ujian');
		for period_obj in self.browse(cr, uid, ids, context=context):
			current_peserta_ids = ruang_peserta_pool.search(cr, uid, [('periode_id','=',period_obj.id)])
			ruang_peserta_pool.unlink(cr, uid, current_peserta_ids, context=context)
			for jadwal_obj in period_obj.jadwal_ids :
				for tempat_obj in period_obj.tempat_ids :
					print "jadwal_obj : ",jadwal_obj.jadwal_ujian_id.name
					print "tempat_obj : ",tempat_obj.tempat_ujian_id.name
					print "period_obj : ",period_obj.name
					self.do_generate_by_kategori(cr,uid, period_obj,tempat_obj,jadwal_obj,None,context=None)
		return True
	def action_generate_venue_by_type(self, cr, uid, ids,kategori_sarjana, context=None):
		print "action_generate_venue..."
		ruang_peserta_pool = self.pool.get('konfigurasi.periode.peserta.ujian');
		for period_obj in self.browse(cr, uid, ids, context=context):
			if kategori_sarjana:
				current_peserta_ids = ruang_peserta_pool.search(cr, uid, [('periode_id','=',period_obj.id), ('kategori_sarjana','=',kategori_sarjana)])
			else :
				current_peserta_ids = ruang_peserta_pool.search(cr, uid, [('periode_id','=',period_obj.id)])
			ruang_peserta_pool.unlink(cr, uid, current_peserta_ids, context=context)
			for jadwal_obj in period_obj.jadwal_ids :
				for tempat_obj in period_obj.tempat_ids :
					print "jadwal_obj : ",jadwal_obj.jadwal_ujian_id.name
					print "tempat_obj : ",tempat_obj.tempat_ujian_id.name
					print "period_obj : ",period_obj.name
					self.do_generate_by_kategori(cr,uid, period_obj,tempat_obj,jadwal_obj,kategori_sarjana,context=None)

	def do_generate_by_kategori(self, cr, uid, period_obj,tempat_obj,jadwal_obj,kategori_sarjana, context=None):
		pendaftaran_pool = self.pool.get('pendaftaran.upkp.ud')
		ruang_peserta_pool = self.pool.get('konfigurasi.periode.peserta.ujian');
		if tempat_obj:
			pendaftar_ids = []
			if kategori_sarjana :
				pendaftar_ids = pendaftaran_pool.search(cr, uid, [('tempat_ujian_id', '=', tempat_obj.tempat_ujian_id.id),
																		   ('jadwal_ujian_id', '=', jadwal_obj.jadwal_ujian_id.id),
																		   ('periode_id','=',period_obj.id),
																		   ('state','=','confirm') ,
                                                                            ('kategori_sarjana','=',kategori_sarjana) ,
																		   ('name','!=',False)],
																order='kategori_sarjana desc,name asc')
			else :
				pendaftar_ids = pendaftaran_pool.search(cr, uid, [('tempat_ujian_id', '=', tempat_obj.tempat_ujian_id.id),
																		   ('jadwal_ujian_id', '=', jadwal_obj.jadwal_ujian_id.id),
																		   ('periode_id','=',period_obj.id),
																		   ('state','=','confirm') ,
																		   ('name','!=',False)],
																order='name asc')
			if pendaftar_ids and len(pendaftar_ids) > 0:
						print "len(pendaftar_ids)  : ",len(pendaftar_ids)
						jml_pendaftar = len(pendaftar_ids)
						pendaftar_idx=0
						for ruang_obj in tempat_obj.tempat_ujian_id.ruang_ids :
							print "ruang_obj : ",ruang_obj
							break_ruang = False
							if kategori_sarjana and kategori_sarjana == 'Non Sarjana' :

								for perserta_obj in period_obj.peserta_ids :
									if perserta_obj.ruang_id.id == ruang_obj.id and perserta_obj.jadwal_ujian_id.id == jadwal_obj.id:
										break_ruang=True
										break

							if break_ruang :
								continue;

							for baris in range(0,ruang_obj.baris):
								for kolom in range(0,ruang_obj.kolom):
									if pendaftar_idx < jml_pendaftar :
										pendaftar_id = pendaftar_ids[pendaftar_idx]

										if pendaftar_id :
											print str(pendaftar_idx),".Pendaftar : ",str(pendaftar_id) ," bertempat di [ ",str(baris),",",str(kolom)," ]"
											peserta =  {}
											peserta.update({
													   'periode_id':period_obj.id,
														'upkp_ud_id':pendaftar_id,
														'tempat_ujian_id':tempat_obj.tempat_ujian_id.id,
														'jadwal_ujian_id':jadwal_obj.jadwal_ujian_id.id,
														'ruang_id':ruang_obj.id,
														'baris':baris,
														'kolom':kolom,
														'kategori_sarjana':kategori_sarjana
											})
											ruang_peserta_id = ruang_peserta_pool.create(cr, uid, peserta,context)
											pendaftar_idx+=1

									else :
										break;

							if pendaftar_idx > jml_pendaftar :
								break;

konfigurasi_periode_upkp_ud()

class konfigurasi_periode_jadwal_ujian(osv.Model):
	_name = 'konfigurasi.periode.jadwal.ujian'
	_description ='Periode Jadwal Ujian'

	_columns = {
		'jadwal_ujian_id' : fields.many2one('konfigurasi.jadwal.ujian', 'Jadwal Ujian', required=True),
		'periode_id' : fields.many2one('konfigurasi.periode.upkp.ud', 'Periode', ),
	}
konfigurasi_periode_jadwal_ujian()

class konfigurasi_periode_tempat_ujian(osv.Model):
	_name = 'konfigurasi.periode.tempat.ujian'
	_description ='Periode Tempat Ujian'

	_columns = {
		'tempat_ujian_id' : fields.many2one('konfigurasi.tempat.ujian', 'Tempat Ujian', required=True),
		'periode_id' : fields.many2one('konfigurasi.periode.upkp.ud', 'Periode', ),
	}
konfigurasi_periode_tempat_ujian()

class konfigurasi_periode_peserta_ujian(osv.Model):
	_name = 'konfigurasi.periode.peserta.ujian'
	_description ='Periode Peserta Ujian'

	_columns = {
		'upkp_ud_id' : fields.many2one('pendaftaran.upkp.ud', 'Peserta UPKP UD', required=True),
		'periode_id' : fields.many2one('konfigurasi.periode.upkp.ud', 'Periode', ),
		'jadwal_ujian_id' : fields.many2one('konfigurasi.jadwal.ujian', 'Jadwal Ujian',),
		'tempat_ujian_id' : fields.many2one('konfigurasi.tempat.ujian', 'Tempat Ujian'),
		'ruang_id' : fields.many2one('konfigurasi.ruang.tempat.ujian', 'Ruang', ),
		'baris'     : fields.integer('Baris',required=True),
		'kolom'     : fields.integer('Kolom',required=True),
		'kategori_sarjana': fields.selection(KATEGORI_SARJANA_STATE, 'Sarjana/Non Sarjana',required=False),
	}
konfigurasi_periode_peserta_ujian()

class konfigurasi_jadwal_ujian(osv.Model):
	_name = 'konfigurasi.jadwal.ujian'
	_description ='Jadwal Ujian'

	_columns = {
		'name'     : fields.char('Sesi',size=180,required=True),
		'tgl_mulai':fields.datetime('Tanggal Mulai',required=True),
		'tgl_selesai':fields.datetime('Tanggal Selesai',required=True),
	}

konfigurasi_jadwal_ujian()

class konfigurasi_tempat_ujian(osv.Model):
	_name = 'konfigurasi.tempat.ujian'
	_description ='Tempat Ujian'

	_columns = {
		'name'     : fields.char('Nama Tempat',size=150,required=True),
		'alamat'     : fields.text('Alamat',required=True),
		'ruang_ids': fields.one2many('konfigurasi.ruang.tempat.ujian', 'tempat_ujian_id', 'Daftar Ruang'),

	}

konfigurasi_tempat_ujian()
class konfigurasi_ruang_tempat_ujian(osv.Model):
	_name = 'konfigurasi.ruang.tempat.ujian'
	_description ='konfigurasi ruang Tempat Ujian'

	_columns = {
		'tempat_ujian_id' : fields.many2one('konfigurasi.tempat.ujian', 'Tempat Ujian', required=True),
		'name'     : fields.char('Nama Ruang',size=150,required=True),
		'jumlah_kuota'     : fields.integer('Jumlah Kuota',required=True),
		 'baris'     : fields.integer('Baris',required=True),
		'kolom'     : fields.integer('Baris',required=True),
	}
konfigurasi_ruang_tempat_ujian()
class konfigurasi_periode_materi_ujian(osv.Model):
	_name = 'konfigurasi.periode.materi.ujian'
	_description ='Materi Ujian Ujian'

	_columns = {
		'materi_id' : fields.many2one('konfigurasi.materi.ujian', 'Materi Ujian', required=True),
		'periode_id' : fields.many2one('konfigurasi.periode.upkp.ud', 'Periode', ),
	}
konfigurasi_periode_materi_ujian()