import time
import xlwt
import cStringIO
from xlwt import Workbook, Formula
from report_engine_xls import report_xls
from upkp_ud_nominatif_report_parser import upkp_ud_nominatif_report_parser

class upkp_ud_nominatif_report_xls_generator(report_xls):
	
    def generate_xls_report(self, parser, filters, obj, workbook):
	worksheet = workbook.add_sheet(('Laporan Nominatif'))

        worksheet.panes_frozen = True
        worksheet.remove_splits = True
        worksheet.portrait = False # Landscape
        worksheet.fit_wiresult_datah_to_pages = 1
        worksheet.col(1).wiresult_datah = len("ABCDEFG")*1024


        
        # Styles (It's used for writing rows / headers)
        info_left_style = xlwt.easyxf('font: height 220, name Arial, colour_index black, bold on, italic off; align: wrap on, , vert centre, horiz left;pattern: pattern solid, fore_color white;', num_format_str='#,##0.00;(#,##0.00)')
        info_style = xlwt.easyxf('font: height 220, name Arial, colour_index black, bold on, italic off; align: wrap on, , vert centre, horiz center;pattern: pattern solid, fore_color white;', num_format_str='#,##0.00;(#,##0.00)')
        row_normal_style=  xlwt.easyxf("borders: top thin, bottom thin, left thin, right thin;",num_format_str='#,##0.00;(#,##0.00)')
        row_normal_odd_style=  xlwt.easyxf('pattern: pattern solid, fore_color silver_ega;',num_format_str='#,##0.00;(#,##0.00)')
        top_style = xlwt.easyxf('font: height 200, name Arial, colour_index black, bold on, italic off; align: wrap on, vert centre, horiz center;pattern: pattern solid, fore_color white;', num_format_str='#,##0.00;(#,##0.00)')
        header_style = xlwt.easyxf('font: height 200, name Arial, colour_index black, bold on, italic off; align: wrap on, vert centre, horiz center;pattern: pattern solid, fore_color silver_ega;', num_format_str='#,##0.00;(#,##0.00)')
        header_w_border_style = xlwt.easyxf('font: height 200, name Arial, colour_index black, bold on, italic off; align: wrap on, vert centre, horiz center;pattern: pattern solid, fore_color silver_ega;borders: top thin, bottom thin, left thin, right thin;', num_format_str='#,##0.00;(#,##0.00)')
        sum_style = xlwt.easyxf('font: height 200, name Arial, colour_index black, bold on, italic off; align: wrap on, vert centre;pattern: pattern solid, fore_color white;', num_format_str='#,##0.00;(#,##0.00)')
        int_number_style=  xlwt.easyxf("borders: top thin, bottom thin, left thin, right thin;",num_format_str='#,##0;(#,##0)')
		
	# Specifying columns, the order doesn't matter
	# lamda d,f,p: is a function who has filter,data,parser as the parameters it is expected to the value of the column
        cols_specs = [
	    # Infos
	    ('Company', 3, 0, 'text', lambda x, d, p: p.get_company_title('Kabupaten/Kota : ',filters)),
	    ('Title',  15, 0, 'text', lambda x, d, p: 'Laporan Nominatif'),

	    ('Period',  3, 0, 'text', lambda x, d, p: p.get_period_title('Periode : ',filters)),
	    # Main Headers / Rows
	    ('No', 1, 17, 'number', lambda x, d, p: 0,xlwt.Row.set_cell_number,int_number_style),
	    ('Nama', 1, 80, 'text', lambda x, d, p:  d.pegawai_id.name or ''),
        ('NIP', 1, 75, 'text', lambda x, d, p:  d.nip or ''),
        ('Tanggal Lahir', 1, 65, 'text', lambda x, d, p:  d.tanggal_lahir or ''),
        ('Pangkat Golongan Ruang', 1, 65, 'text', lambda x, d, p:  d.golongan_id and d.golongan_id.description or ''),
        ('Tingkat Ujian Dinas', 1, 70, 'text', lambda x, d, p:  d.tingkat_ujian_dinas or ''),
        ('Pendidikan Terakhir', 1, 75, 'text', lambda x, d, p:  d.pendidikan_terakhir or ''),
        ('Jabatan', 1, 75, 'text', lambda x, d, p:  d.job_id and d.job_id.name or ''),
        ('Unit Kerja', 1, 75, 'text', lambda x, d, p: d.department_id and d.department_id.name or ''),
        ('Kabupaten/Kota', 1, 75, 'text', lambda x, d, p: d.company_id and d.company_id.name or ''),
        ('DP3 1', 1, 30, 'number', lambda x, d, p: d.nilai_skp_1,xlwt.Row.set_cell_number,int_number_style),
        ('DP3 2', 1, 30, 'number', lambda x, d, p: d.nilai_skp_2,xlwt.Row.set_cell_number,int_number_style),
        ('Status', 1, 45, 'text', lambda x, d, p:  p.get_state_label(d.state or '') ),
        ('No Ujian', 1, 50, 'text', lambda x, d, p:  d.name or ''),
        ('No Surat Lulus', 1, 50, 'text', lambda x, d, p:  d.no_surat_lulus or ''),
        ('No SK', 1, 50, 'text', lambda x, d, p:  d.kepgub_pengumuman_kelulusan or ''),



	    #('Jumlah Target', 1, 40, 'number', lambda x, d, p: d['pro_all'],xlwt.Row.set_cell_number,int_number_style),
	    
        # Misc
	    ('single_empty_column', 1, 0, 'text', lambda x, d, p: ''),
	    ('triple_empty_column', 3, 0, 'text', lambda x, d, p: ''),
	    ('quadruple_empty_column', 4, 0, 'text', lambda x, d, p: ''),
	]
    
			 
        if filters['tipe_ujian'] == 'ud' :
            row_spec_value = ['No','Nama','NIP','Tanggal Lahir','Pangkat Golongan Ruang',
                              'Tingkat Ujian Dinas','Jabatan','Unit Kerja','Kabupaten/Kota',
                              'DP3 1','DP3 2','Status','No Ujian','No Surat Lulus','No SK']
        else :
            row_spec_value = ['No','Nama','NIP','Tanggal Lahir','Pangkat Golongan Ruang',
                              'Pendidikan Terakhir','Jabatan','Unit Kerja','Kabupaten/Kota',
                              'DP3 1','DP3 2','Status','No Ujian','No Surat Lulus','No SK']

       
        # Row templates (Order Matters, this joins the columns that are specified in the second parameter)
        company_template = self.xls_row_template(cols_specs, ['Company'])
        title_template = self.xls_row_template(cols_specs, ['Title'])
        title_upkp_template = self.xls_row_template(cols_specs, ['Title UPKP'])
        period_template = self.xls_row_template(cols_specs, ['Period'])
        row_template = self.xls_row_template(cols_specs,row_spec_value)

        empty_row_template = self.xls_row_template(cols_specs, ['single_empty_column'])
        
        # Write infos
        # xls_write_row(worksheet, filters, data parser, row_number, template, style)
        
        row_count=0;
        self.xls_write_row(worksheet, filters, None, parser, row_count, period_template, info_left_style)
        row_count+=1
        self.xls_write_row(worksheet, filters, None, parser, row_count,company_template , info_left_style)
        row_count+=2
        self.xls_write_row(worksheet, filters, None, parser, row_count, title_template, info_style)

            # Write headers (It uses the first parameter of cols_specs)
        row_count+=1
        self.xls_write_row_header(worksheet, row_count, row_template, header_w_border_style, set_column_size=True)
        style = row_normal_style
        row_count+=1
        idx=1
        result = parser.get_upkp_ud_nominatif_report_raw(filters); #ujian dinas sheet
        for upkp_ud_nominatif_data in result:
            # Write Rows
                style = row_normal_style
                self.xls_write_row_with_indeks(worksheet, filters, upkp_ud_nominatif_data, parser, row_count, row_template, style,idx)
                idx+=1
                row_count+=1





    # Override from report_engine_xls.py	
    def create_source_xls(self, cr, uid, ids, filters, report_xml, context=None): 
        if not context: context = {}
	
	# Avoiding context's values change
        context_clone = context.copy()
	
        rml_parser = self.parser(cr, uid, self.name2, context=context_clone)
        objects = self.getObjects(cr, uid, ids, context=context_clone)
        rml_parser.set_context(objects, filters, ids, 'xls')
        io = cStringIO.StringIO()
        workbook = xlwt.Workbook(encoding='utf-8')
        self.generate_xls_report(rml_parser, filters, rml_parser.localcontext['objects'], workbook)
        workbook.save(io)
        io.seek(0)
        return (io.read(), 'xls')

#Start the reporting service
upkp_ud_nominatif_report_xls_generator(
    #name (will be referred from upkp_ud_nominatif_report.py, must add "report." as prefix)
    'report.upkp.ud.nominatif.xls',
    #model
    'upkp.ud.nominatif.report',
    #file
    'addons/df_upkp_ud_nominatif_report/report/upkp_ud_nominatif_report.xls',
    #parser
    parser=upkp_ud_nominatif_report_parser,
    #header
    header=True
)