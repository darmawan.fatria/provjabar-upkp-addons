from openerp.osv import fields, osv
from openerp.addons.df_upkp_ud.konfigurasi_periode_upkp_ud import PERIODE_BULAN_LIST


class upkp_ud_nominatif_report(osv.osv_memory):
    
    _name = "upkp.ud.nominatif.report"
    _columns = {
        'company_id'        : fields.many2one('res.company', 'Kabupaten/Kota',domain="[('is_main_company','=',True)]"),
        'periode_id'        : fields.many2one('konfigurasi.periode.upkp.ud', 'Periode',required=True),

    }

    
    def get_upkp_ud_nominatif_report(self, cr, uid, ids, context={}):
        value = self.read(cr, uid, ids)[0]
        tipe_ujian = self.browse(cr,uid,ids)[0].periode_id.tipe_id.tipe_ujian
        datas = {
            'ids': context.get('active_ids',[]),
            'model': 'upkp.ud.nominatif.report',
            'form': value,
            'tipe_ujian':tipe_ujian
        }
	
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'upkp.ud.nominatif.xls',
            'report_type': 'webkit',
            'datas': datas,
        }
    
upkp_ud_nominatif_report()
