from openerp.osv import fields, osv
from openerp.addons.df_upkp_ud.partner_employee import KATEGORI_SARJANA_STATE
from openerp.addons.df_upkp_ud.partner_employee import TINGKAT_UJIAN_DINAS_STATE
import time

class upkp_ud_kehadiran_report(osv.osv_memory):
    
    _name = "upkp.ud.kehadiran.report"
    _columns = {
        'company_id'        : fields.many2one('res.company', 'Kabupaten/Kota',domain="[('is_main_company','=',True)]"),
        'periode_id'        : fields.many2one('konfigurasi.periode.upkp.ud', 'Periode',required=True),
        'kategori_sarjana': fields.selection(KATEGORI_SARJANA_STATE, 'Sarjana/Non Sarjana',required=False),
        'tingkat_ujian_dinas': fields.selection(TINGKAT_UJIAN_DINAS_STATE,"Tingkat Ujian Dinas", required=False),


    }

    
    def get_upkp_ud_kehadiran_report(self, cr, uid, ids, context={}):
        value = self.read(cr, uid, ids)[0]
        tipe_ujian = self.browse(cr,uid,ids)[0].periode_id.tipe_id.tipe_ujian
        datas = {
            'ids': context.get('active_ids',[]),
            'model': 'upkp.ud.kehadiran.report',
            'form': value,
            'tipe_ujian':tipe_ujian
        }
	
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'upkp.ud.kehadiran.xls',
            'report_type': 'webkit',
            'datas': datas,
        }
    
upkp_ud_kehadiran_report()
