import openerp.pooler
from openerp.report import report_sxw
from openerp.addons.df_upkp_ud.pendaftaran_upkp_ud import PENDAFTARAN_UJIAN_STATE
from openerp.addons.df_upkp_ud.konfigurasi_periode_upkp_ud import PERIODE_BULAN_LIST
#import locale

class upkp_ud_kehadiran_report_parser(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context=None):
        super(upkp_ud_kehadiran_report_parser, self).__init__(cr, uid, name, context=context)

    def get_upkp_ud_kehadiran_report_raw(self,filters,context=None):


        domain = []
        kategori_sarjana=False;
        tingkat_ujian_dinas=False;
        if filters['form']['periode_id']:
            periode_id = filters['form']['periode_id'][0]
            domain.append(('periode_id','=',periode_id))

        if filters['form']['company_id']:
            company_id = filters['form']['company_id'][0]
            domain.append(('parent_company_id','=',company_id))

        if filters['form']['kategori_sarjana']:
            kategori_sarjana = filters['form']['kategori_sarjana']
        if filters['form']['tingkat_ujian_dinas']:
            tingkat_ujian_dinas = filters['form']['tingkat_ujian_dinas']

        if kategori_sarjana:
            domain.append(('kategori_sarjana','=',kategori_sarjana))
        if tingkat_ujian_dinas:
            domain.append(('tingkat_ujian_dinas','=',tingkat_ujian_dinas))

        print "domain : ",domain
        obj_pool = self.pool.get('pendaftaran.upkp.ud')
        result_ids=obj_pool.search(self.cr, self.uid,domain , context=None)
        results = obj_pool.browse(self.cr, self.uid, result_ids)

        return results

    '''
    def get_upkp_ud_kehadiran_report_raw(self,filters,tipe_ujian,context=None):

        domain = [('tipe_ujian','=',tipe_ujian),]
        period_domain = []
        periode_pendaftaran_ids = []
        periode_pool = self.pool.get('konfigurasi.periode.upkp.ud')
        if filters['form']['periode_bulan'] and filters['form']['periode_tahun']:
            periode_bulan = filters['form']['periode_bulan']
            periode_tahun = filters['form']['periode_tahun']
            period_domain.append(('periode_tahun','=',periode_tahun))
            period_domain.append(('periode_bulan','=',periode_bulan))
            periode_pendaftaran_ids=periode_pool.search(self.cr, self.uid,period_domain , context=None)
            domain.append(('periode_id','in',periode_pendaftaran_ids))
        if filters['form']['company_id']:
            company_id = filters['form']['company_id'][0]
            domain.append(('company_id','=',company_id))

        obj_pool = self.pool.get('pendaftaran.upkp.ud')
        result_ids=obj_pool.search(self.cr, self.uid,domain , context=None)
        results = obj_pool.browse(self.cr, self.uid, result_ids)

        return results
    '''
    def get_period_title(self,prefix,filters):

        title='-';

        if filters['form']['periode_bulan'] and filters['form']['periode_tahun']:
            title = self.get_period_month_label(filters['form']['periode_bulan'])+ " - " + filters['form']['periode_tahun']
        return prefix+title
    def get_company_title(self,prefix,filters):

        title='-';

        if filters['form']['company_id']:
            title = filters['form']['company_id'][1]
        return prefix+title
    def get_title(self,prefix,filters):

        title='-';

        if filters['form']['company_id']:
            title = filters['form']['company_id'][1]
        return prefix+title
    def get_period_month_label(self,val):

        label=val
        for data in PERIODE_BULAN_LIST :
            if data[0] == val:
                return data[1]

        return label
    def get_state_label(self,val):

        label=val
        for data in PENDAFTARAN_UJIAN_STATE :
            if data[0] == val:
                return data[1]

        return label
        
   
    
